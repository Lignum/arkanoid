#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#include "GUI/GUIContainer.h"
#include "GUI/Cursor.h"
#include "GUI/GUIImage.h"
#include "GUI/Button.h"
#include "GUI/Text.h"

#include "Arkanoid/GameObjectManager.h"

#include "TrackManager.h"
#include "SoundFXManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class PlayState : public Ogre::Singleton<PlayState>, public GameState{
    
    private:
        
        //SOUND
        SoundFXPtr _simpleEffect;
        
        int _stage;
        int _ballsNum;
        int _score;
        int _highScore;
        int _totalScore;
        int _round;
        
        bool _selectState;
        bool _playState;
        bool _newStage;
        bool _gameOver;
        
        void CreateGui();
        void resetPoint();
        
        void loadStages();
        void loadStage(int num);
        
        void selectState();
        void playState(const Ogre::FrameEvent& evt);
        void newStage(const Ogre::FrameEvent& evt);
        
        void overRetry();
        void overExit();
        void overSave();

    public:
         GameObjectManager* _gom;

        PlayState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

    protected:

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif
