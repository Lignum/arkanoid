/* 
 * File:   Score.h
 * Author: victor
 *
 * Created on 13 de febrero de 2015, 18:41
 */

#ifndef SCORE_H
#define	SCORE_H

class Score{
    
public:
    char name[20];
    int score; 
    
    Score(){
    }
    
    Score(char n[20], int sco){
        for(int i=0;i<20;i++){
            name[i] = n[i];
        }
        //name = n;
        score = sco;
    }
    
    
    bool operator<(const Score &rhs) const{ 
        return score < rhs.score; 
    }
};
        


#endif	/* SCORE_H */

