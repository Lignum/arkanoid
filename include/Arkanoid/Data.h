/* 
 * File:   Data.h
 * Author: victor
 *
 * Created on 4 de febrero de 2015, 16:56
 */

#ifndef DATA_H
#define	DATA_H

#include <Ogre.h>
#include "../File/ReadWrite.h"
#include "GameObject.h"

#include <dirent.h>


class Data{
    
private:
    
    
public:
    
    
    class Point{
        
        public:
            
            Vector2 _pos;
            bool _double;
            Vector3 _color;
            
            Point(){

            }

            Point(Cube* cube){
                
                _pos = Vector2(cube->getPosition().x/0.75, cube->getPosition().y/0.75);
                _double = cube->isDouble();
                _color = cube->getColor();                    

            }
        };
    
    std::string _name;
    ReadWrite<Point> _save;
    ReadWrite<std::string> _names;
    
    Data(){
        
        //_name = file;
        
        //ReadWrite<std::vector<D> > save ("saves/stages.save");
        
        _names = ReadWrite<std::string> ("saves/name.save");
        //_save = ReadWrite<Point> ("saves/"+_name+".save");
        //_save = ReadWrite<std::vector<Points> > ("saves/"+file+".save");
        
        
        
    }
    
    void addStage(std::string na, std::vector<Point> base){
        
        
        _save = ReadWrite<Point> ("saves/"+na+".save");
        std::vector<Point> cubes;
        
        if(!_save.remove()){
            
            
            std::vector<std::string> names = getNames();
            std::string name = "";
            for (long unsigned int i=0; i<names.size(); i++){
                name = name+names[i]+"\n";
            }
            
            name = name + na;
                
            ofstream myfile;
            myfile.open ("saves/name.save");
            myfile << name;
            myfile.close();
            
            //std::cout <<  "----------------------------------------" << _name << std::endl;
            //_names.write(_name.c_str());
            
        }
        for(long unsigned int i=0; i<base.size(); i++){
            cubes.push_back(base[i]);
            _save.write(cubes[i]);
        }
        
    }
    
    std::vector<std::string> getNames(){
        
        ifstream file("saves/name.save");
        std::vector<std::string> names;
        std::string name;
        while(file >> name){
            names.push_back(name);
        }
        return names;
    }
    
    std::vector<Point> getStage(std::string na){
        
        ReadWrite<Point> ss = ReadWrite<Point> ("saves/"+na+".save");
        return ss.read();
    }
    
    std::vector<std::vector<Point> > getAllStages(){
        
        std::vector<std::vector<Point> > stages;
        
        std::vector<std::string> names = getNames();
        for (long unsigned int i=0; i<names.size(); i++){            
            ReadWrite<Point> ss = ReadWrite<Point> ("saves/"+names[i]+".save");
            stages.push_back(ss.read());
        }
        
        return stages;
    }
    
};

#endif	/* DATA_H */

