/* 
 * File:   GameObjectManager.h
 * Author: victor
 *
 * Created on 28 de enero de 2015, 19:34
 */

#ifndef GAMEOBJECTMANAGER_H
#define	GAMEOBJECTMANAGER_H

#include <Ogre.h>

#include "GameObject.h"

using namespace Ogre;

class GameObjectManager{
    
    SceneManager* _sceneMgr;
    std::vector<GameObject*> _gameObjects;
    
public:
    GameObjectManager(SceneManager* sce);
    
    void createObject(const String& name, const String& mesh, Real x, Real y, Real z);
    void addObject(GameObject* object);
    GameObject* getObjectByName(const String& name);
    GameObject* getObject(const int num);
        
    void clear();
    void destroyObject(int i);    
    void destroyObject(int start, int end);
    void setVisible(bool visible);
    
    std::vector<GameObject*> getObjects();
    
    void update(Real delta);
    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);
    
    SceneManager* getSceneManager();
    
};


#endif	/* GAMEOBJECTMANAGER_H */

