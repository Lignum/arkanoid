/* 
 * File:   PowerUp.h
 * Author: victor
 *
 * Created on 15 de febrero de 2015, 15:00
 */

#ifndef POWERUP_H
#define	POWERUP_H

#include "GameObject.h"

class PowerUp : public GameObject{
    
    private:
        
        static const int _speed = 4;
        int _type;
    
    public:
        
        static const int T_LONG = 0;
        static const int T_DOUBLE = 1;
        static const int T_LIFE = 2;
        static const int T_STICKY = 3;
        
        PowerUp(SceneManager* sce, Real x, Real y);
        
        void update(Real Delta);
        
        int getType();
    
};

#endif	/* POWERUP_H */

