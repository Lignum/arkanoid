/* 
 * File:   Cube.h
 * Author: victor
 *
 * Created on 29 de enero de 2015, 14:58
 */

#ifndef CUBE_H
#define	CUBE_H

#include "GameObject.h"

class Cube : public GameObject{
    
public:
    
    Cube(SceneManager* sce, Real x, Real y, bool doubl, Real r, Real g, Real b);
    Cube(GameObjectManager* gom, Real x, Real y, bool doubl,Real r, Real g, Real b);
    
    ~Cube();
    
    void update(Real delta);
    
    void setPosition(Real x, Real y);
    void setDouble(bool dou);
    
    bool isDouble();
    
};

#endif	/* CUBE_H */

