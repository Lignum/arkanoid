/* 
 * File:   Ball.h
 * Author: victor
 *
 * Created on 2 de febrero de 2015, 15:16
 */

#ifndef BALL_H
#define	BALL_H


#include "GameObject.h"

class Ball : public GameObject{

private:
    Real _speed;
    Vector2 _dir;
    
    int _lastCol;
    Real _sticky;
    
    void init();
    
public:    

    Ball(SceneManager* sce, Real x, Real y);
    Ball(GameObjectManager* gom, Real x, Real y);
    
    ~Ball();
    
    void update(Real delta);
    void start();
    void start(Real speed, Vector2 dir);
    
    void setPosition(Real x, Real y);
    void translate(Real x, Real y);
    void translate(Vector2 vec);
    
    int collide(GameObject* object, bool repit);
    
    Real getSpeed();
    void setSticky(Real sticky);
    Real getSticky();
    
    void setDirection(Vector2 vec);
    Vector2 getDirection();
    void addDirection(Vector2 vec);
    
    void bounce(Vector2 vec);
    
};



#endif	/* BALL_H */

