/* 
 * File:   Paddle.h
 * Author: victor
 *
 * Created on 10 de febrero de 2015, 17:58
 */

#ifndef PADDLE_H
#define	PADDLE_H

#include "GameObject.h"
#include "PowerUp.h"

class Paddle  : public GameObject{
    
    private:
        
        int _dir;
        Real _speed;
        
        bool _sticky;
        
        Real _doubleTime;
        Real _stickyTime;
    
    public:
        
        Paddle(SceneManager* gom);
        
        void update(Real delta);
        void keyPressed(const OIS::KeyEvent &e);
        void keyReleased(const OIS::KeyEvent &e);
        
        void powerUp(int type);
        
        bool isSticky();
    
};

#endif	/* PADDLE_H */

