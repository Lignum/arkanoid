/* 
 * File:   Base.h
 * Author: victor
 *
 * Created on 28 de enero de 2015, 20:01
 */

#ifndef BASE_H
#define	BASE_H

#include "GameObject.h"
#include "Ball.h"
#include "Cube.h"
#include "Data.h"
#include "Paddle.h"
#include "PowerUp.h"


#include "TrackManager.h"
#include "SoundFXManager.h"

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class Base : public GameObject{
    
private:
    Real _CubeAnc;
    Real _CubeAlt;
    
    std::vector<GameObject*> _walls;
    std::vector<Cube*> _cubes;
    std::vector<PowerUp*> _powerUps;
    std::vector<Ball*> _balls;
    Paddle* _paddle;
    
    int _score;
    bool _life;
    
    //SOUND
    SoundFXManager* _pSoundFXManager;
    SoundFXPtr _wallCollison;
    SoundFXPtr _boxCollison;
    
    void init();
    
    bool paddleCollision(Real delta, Ball* ball);
    bool wallsCollision(Real delta, Ball* ball);
    bool cubesCollision(Real delta, Ball* ball);
    
public:

    Base(SceneManager* sce, const String& name);
    Base(GameObjectManager* gom, const String& name);
    Base(GameObjectManager* gom, std::vector<Data::Point> data);
    
    ~Base();
    
    void update(Real delta);
    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);
    
    void start();
    
    void clear();

    void createCube(int x, int y, bool doubl, Real r, Real g, Real b);
    void createCube(Vector2 pos, bool doubl, Vector3 color);
    void setCubePosition(int cube, int x, int y);
    void createPowerUp(int x, int y);
    void startBall();
    void createBall(Real x, Real y);
    void duplicateBall();
    
    bool hasLife();
    
    void setCastShadows(bool base, bool cubes);
    
    int getBallsNum();
    int getCubesNum();
    int getScore();
    
    void destroyCube(int n);
    void destroyCube(int x, int y);
    Cube* getCube(int x, int y);
    
    std::vector<Data::Point> getCubesData();
        
    std::vector<Cube*> getCubes();
    std::vector<Ball*> getBalls();
    
};

#endif	/* BASE_H */

