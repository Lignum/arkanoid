/* 
 * File:   GUIContainer.h
 * Author: victor
 *
 * Created on 10 de diciembre de 2014, 23:18
 */

#ifndef GUICONTAINER_H
#define	GUICONTAINER_H

#include <OgrePanelOverlayElement.h>
#include <vector>

#include "GUIImage.h"
#include "Cursor.h"
#include "Button.h"
#include "Text.h"
#include "TextField.h"
#include "Slider.h"
#include "GUIGroup.h"
 
using namespace Ogre;

class GUIContainer{
    
    private:
        
        static Overlay* _overlay;
        static Overlay* _cursorOverlay;
        static Cursor* _cursor;
        
        static std::vector<GUIImage*> _GUIImages;
        static std::vector<Button*> _buttons;
        static std::vector<TextField*> _textFields;
        static std::vector<Text*> _texts;
        static std::vector<Slider*> _sliders;
        
        static std::vector<Element*> _elements;
        
        
        static std::vector<GUIGroup*> _groups;
        
        static bool _enable;
        static bool _firstFrame;
        
        static Real _wWidth;
        static Real _wHeight;
        
        
        static Real _charHeight;
        static String _textFont;
        static ColourValue _textColor;
        
        
        static void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
        static bool cursorColision(int x, int y, int witdh, int height){
            
            if(_cursor->getX()<x) return false;
            if(_cursor->getY()<y) return false;
            if(_cursor->getX()>x+witdh) return false;
            if(_cursor->getY()>y+height) return false;
            
            return true;
        }

        
    public:
        static void init(Viewport* viewport){
            
            _overlay = OverlayManager::getSingletonPtr()->create("GUI");
            _overlay->setZOrder(600);
            _overlay->show();
            
            _cursorOverlay = OverlayManager::getSingletonPtr()->create("CURSOROV");
            _cursorOverlay->setZOrder(649);
            _cursorOverlay->show();
            
            _cursor = NULL;            
            _buttons.reserve(10);
            
            _charHeight = -1;
            _textFont = "";
            _textColor = ColourValue::ZERO;
            
            _enable = true;
            _firstFrame = true;
            setDimensions(viewport->getActualWidth(), viewport->getActualHeight());
        }
        
       

        ~GUIContainer(void) {}
        
        static void update(){
            
            if(_firstFrame){
                for (long unsigned int i=0; i < _texts.size(); i++){                    
                    _texts[i]->update();                    
                }
                for (long unsigned int i=0; i < _groups.size(); i++){           
                    _groups[i]->update();                    
                }
                _firstFrame = false;
            }

        }
        
        static void setVisible(bool visible){
            if(visible) {
                _overlay->show();
            } else {
                _overlay->hide();
            }
        }
        
        static void setAllvisible(bool visible){
            
            _cursor->setVisible(visible);
            
            for (long unsigned int i=0; i<_GUIImages.size(); i++){
               _GUIImages[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_buttons.size(); i++){
               _buttons[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_texts.size(); i++){
               _texts[i]->setVisible(visible);
            }
                
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_sliders.size(); i++){
                _sliders[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_groups.size(); i++){
                _groups[i]->setAllvisible(visible);
            }
        }
        
        static void unFocus(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                _buttons[i]->setFocus(false);
            }
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setFocus(false);
                _textFields[i]->setSelected(false);
            }
        }
        
        static void showCursor(bool show){
            _cursor->setVisible(show);
        }
        static bool cursorIsVisible(){
            return _cursor->isVisible();
        }
        
        static void createImage(const String& name, const String& texture){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+name)){
                getGUIImage(name)->setVisible(true);
            }else{
                add(new GUIImage(name, texture));
            }
        }
        
        static void createCursor(const String& texture){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/cursor")){
               showCursor(true);
            }else{
                addCursor(new Cursor("cursor", texture));
            }
        }
        
        static void createButton(const String& name, const String& texture, const String& focus, const String& click, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+name)){
                getButton(name)->setVisible(true);
            }else{
                add(new Button(name, texture, focus, click, text));
            }
        }
        
        static void createButton(const String& group, const String& name, const String& texture, const String& focus, const String& click, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getButton(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new Button(group+"/"+name, texture, focus, click, text));
            }
        }
        
        static void createText(const String& name, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+name)){
                getText(name)->setVisible(true);
            }else{
                add(new Text(name, text));
                _firstFrame = true;
            }
        }
        
        static void createText(const String& group, const String& name, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getText(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new Text(group+"/"+name, text));
                _firstFrame = true;
            }
        }
        
        static void createTextField(const String& name, const String& texture, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+name)){
                getTextField(name)->setVisible(true);
            }else{
                add(new TextField(name, texture, text));
                _firstFrame = true;
            }
        }
        
        static void createSlider(const String& name, const String& texture, const String& focus, const String& click, const String& bar, const String& text){
std::cout << "aaaa" << std::endl;
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+name)){
std::cout << "qqqqqqqqqqqqqqqqqqqqq" << std::endl;
                getSlider(name)->setVisible(true);
            }else{
                add(new Slider(name, texture, focus, click, bar, text));
            }
        }
        
        static void createGroup(const String& name){
            if(!OverlayManager::getSingletonPtr()->getByName(name)){
                addGroup(new GUIGroup(name, _wWidth, _wHeight));
            }
        }
        
        static void add(GUIImage* GUIElement){
            _GUIImages.push_back(GUIElement);
            _overlay->add2D(GUIElement->getContainer());
            GUIElement->setDimensions(_wWidth, _wHeight);
        }
        
        static void add(Text* text){
            
            _texts.push_back(text);
            _overlay->add2D(text->getContainer());
                        
            text->setDimensions(_wWidth, _wHeight);
            
            if(_textFont != ""){
                text->setFont(_textFont);
            }
            
            if(_charHeight != -1){
                text->setCharHeight(_charHeight);
            }
            
            if(_textColor != ColourValue::ZERO){
                text->setColor(_textColor);
            }
            //GUIElement->setDimensions(_wWidth, _wH)eight);
        }
        
        static void add(Button* button){
            
            _buttons.push_back(button);
            _overlay->add2D(button->getContainer());
            add(button->getText());
            button->setDimensions(_wWidth, _wHeight);
            button->centerText();
        }
        
        static void add(TextField* field){
            
            _textFields.push_back(field);
            _overlay->add2D(field->getContainer());
            
            add(field->getText());
            field->setDimensions(_wWidth, _wHeight);
        }
        
        static void add(Slider* slider){
            
            add(slider->getBar());
            _sliders.push_back(slider);
            _overlay->add2D(slider->getContainer());
            add(slider->getText());
            add(slider->getValText());
            slider->setDimensions(_wWidth, _wHeight);
        }
        
        static void addGroup(GUIGroup* group){
            _groups.push_back(group);
            
            group->setDefaultTextCharHeight(_charHeight);
            group->setDefaultTextColor(_textColor);
            group->setDefaultTextFont(_textFont);
        }
        
        static void addCursor(Cursor* cursor){
            
            _cursor = cursor;
            _cursorOverlay->add2D(cursor->getContainer()); 
            _cursor->setDimensions(_wWidth, _wHeight);
            
        }
        
        static void setEnable(bool enable){
            _enable = enable;
        }
        
        static void moveCursor(int x, int y){
            
            if(_cursor){
                _cursor->setPosition(x ,y);
                
                //std::vector<Button*>::iterator it;
                for (long unsigned int i=0; i<_buttons.size(); i++){
                    
                    if(_buttons[i]->isVisible()){
                        if(cursorColision(_buttons[i]->getX(), _buttons[i]->getY(),
                                _buttons[i]->getWidth(), _buttons[i]->getHeight())){

                            if(!_buttons[i]->isClick()){                            
                                _buttons[i]->setFocus(true);


                            }
                        }else{
                            _buttons[i]->setFocus(false);
                            _buttons[i]->setClick(false);
                        }
                    }
                    
                }
                
                for (long unsigned int i=0; i<_textFields.size(); i++){
                    
                    if(_textFields[i]->isVisible()){
                        if(cursorColision(_textFields[i]->getX(), _textFields[i]->getY(),
                                _textFields[i]->getWidth(), _textFields[i]->getHeight())){

                            if(!_textFields[i]->isSelected()){                            
                                _textFields[i]->setFocus(true);


                            }
                        }else{
                            _textFields[i]->setFocus(false);
                        }
                    }
                    
                }
                
                for (long unsigned int i=0; i<_sliders.size(); i++){
                    if(_sliders[i]->isVisible()){
                        if(_sliders[i]->isClick()){
                            _sliders[i]->setValPosition(x);
                        }

                        if(cursorColision(_sliders[i]->getX(), _sliders[i]->getY(),
                                _sliders[i]->getWidth(), _sliders[i]->getHeight())){

                            if(!_sliders[i]->isClick()){                            
                                _sliders[i]->setFocus(true);
                            }
                        }else{
                            _sliders[i]->setFocus(false);
                        }
                    }
                    
                }
                
            } else {
                std::cerr << "Error: No cursor found" << std::endl;
            }
        }
        
        static void click(OIS::MouseButtonID id){
            moveCursor(_cursor->getX(), _cursor->getY());
            
            
            if(_cursor){
                if(_cursor->isVisible()){                    
                    
                    for (long unsigned int i=0; i < _groups.size(); i++){                    
                        _groups[i]->click(id);                  
                    }
                    
                    if(id == OIS::MB_Left){
                        for (long unsigned int i=0; i<_buttons.size(); i++){
                            if(_buttons[i]->isFocus()){
                                _buttons[i]->setClick(true);
                            }

                        }

                        for (long unsigned int i=0; i<_textFields.size(); i++){ 

                            _textFields[i]->setSelected(false);

                            if(_textFields[i]->isFocus()){
                                _textFields[i]->setSelected(true);
                            }

                        }
                        
                        for (long unsigned int i=0; i<_sliders.size(); i++){
                            if(_sliders[i]->isFocus()){
                                _sliders[i]->setClick(true);
                            }
                        }
                    }
                }
            }
        }
        
        static void clickReleased(OIS::MouseButtonID id){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                _buttons[i]->setClick(false);

            }

            for (long unsigned int i=0; i<_sliders.size(); i++){
                _sliders[i]->setClick(false);
            }
        }
        
        static int getButtonFocusNum(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if(_buttons[i]->isVisible()){
                    if(_buttons[i]->isFocus()){
                        return i;
                    }
                }
            }
            
            return -1;
        }
        
        static void keyPressed(const OIS::KeyEvent &e){
            
            
            //DOWN
            if(e.key == OIS::KC_DOWN or e.key == OIS::KC_RIGHT){
                unsigned int num = getButtonFocusNum();
                unFocus();
                unsigned int n = 1;
                if((int)num == -1){
                    n = 0;
                    while(!_buttons[n]->isVisible()){
                        n++;
                    }
                    _buttons[n]->setFocus(true);
                }else{
                    if(num < _buttons.size()-1){
                        _buttons[num]->setFocus(false);
                        while(!_buttons[num+n]->isVisible()){
                            n++;
                            if(num+n == _buttons.size()){
                                num = 0;
                                n = 0;
                            }
                        }
                        _buttons[num+n]->setFocus(true);
                    }else{
                        n = 0;
                        _buttons[num]->setFocus(false);
                        while(!_buttons[n]->isVisible()){
                            n++;
                        }
                        _buttons[n]->setFocus(true);                        
                    }
                }
            }
            
            //UP
            if(e.key == OIS::KC_UP or e.key == OIS::KC_LEFT){
                int num = getButtonFocusNum();
                unFocus();
                int n = 1;
                if(num == -1){
                    num = _buttons.size();
                    while(!_buttons[num-n]->isVisible()){
                        n++;
                    }
                    _buttons[num-n]->setFocus(true);
                }else{
                    if(num == 0){
                        _buttons[0]->setFocus(false);
                        while(!_buttons[_buttons.size()-n]->isVisible()){
                            n++;
                        }
                        _buttons[_buttons.size()-n]->setFocus(true);
                    }else{
                        _buttons[num]->setFocus(false);
                        while(!_buttons[num-n]->isVisible()){
                            n++;
                            if(num-n == -1){
                                num = _buttons.size()-1;
                                n = 0;
                            }
                        }
                        _buttons[num-n]->setFocus(true);
                    }
                }
            }
            
            for (long unsigned int i=0; i<_textFields.size(); i++){
                    
                if(_textFields[i]->isSelected()){
                    
                    _textFields[i]->keyPressed((char)e.text);
                }

            }
            //Console::log(e.text);
        }
        
        static void setDefaultTextCharHeight(Real height){
            
            _charHeight = height;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setCharHeight(height);
            }
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextCharHeight(height);
            }
        }
        
        static void setDefaultTextFont(String font){
            
            _textFont = font;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setFont(_textFont);
            }
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextFont(font);
            }
        }
        
        static void setDefaultTextColor(ColourValue color){
            
            _textColor = color;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setColor(color);
            }
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextColor(color);
            }
        }
        
        static GUIImage* getGUIImage(const String& name){
            for (long unsigned int i=0; i<_GUIImages.size(); i++){
                if( _GUIImages[i]->getName() == name){
                    return _GUIImages[i];
                }
            }
            
            return NULL;
        }
        
        static GUIImage* getGUIImage(const int num){
                        
            return _GUIImages[num];
        }
        
        static std::vector<Button*> getButtons(){
            return _buttons;
        }
        
        static Button* getButton(const String& name){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if( _buttons[i]->getName() == name){
                    return _buttons[i];
                }
            }            
            return NULL;
        }
       
        static Button* getButton(const String& group, const String& name){           
            
            for (long unsigned int i=0; i<_groups.size(); i++){
                if( _groups[i]->getName() == group){
                    return _groups[i]->getButton(group+"/"+name);
                }
            }
            
            return NULL;
        }
        
        static Button* getButton(const int num){
                        
            return _buttons[num];
        }
        
        static Text* getText(const String& name){
            for (long unsigned int i=0; i<_texts.size(); i++){
                if( _texts[i]->getName() == name){
                    return _texts[i];
                }
            }
            
            return NULL;
        }
        
        static Text* getText(const String& group, const String& name){
            for (long unsigned int i=0; i<_groups.size(); i++){
                if( _groups[i]->getName() == group){
                    return _groups[i]->getText(group+"/"+name);
                }
            }
            
            return NULL;
        }
        
        static Text* getText(const int num){
                        
            return _texts[num];
        }
        
        static TextField* getTextField(const String& name){
            for (long unsigned int i=0; i<_textFields.size(); i++){
                if( _textFields[i]->getName() == name){
                    return _textFields[i];
                }
            }
            
            return NULL;
        }
        
        static TextField* getTextField(const int num){
                        
            return _textFields[num];
        }
        
        static Slider* getSlider(const String& name){
            for (long unsigned int i=0; i<_sliders.size(); i++){
                if( _sliders[i]->getName() == name){
                    return _sliders[i];
                }
            }
            
            return NULL;
        }
        
        static Slider* getSlider(const int num){
                        
            return _sliders[num];
        }
        
        static GUIGroup* getGroup(const String& name){
            for (long unsigned int i=0; i<_groups.size(); i++){
                if( _groups[i]->getName() == name){
                    return _groups[i];
                }
            }
            
            return NULL;
        }
        
        static GUIGroup* getGroup(const int num){
                        
            return _groups[num];
        }

};

#endif	/* GUICONTAINER_H */

