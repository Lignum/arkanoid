/* 
 * File:   Cursor.h
 * Author: victor
 *
 * Created on 10 de diciembre de 2014, 23:55
 */

#ifndef CURSOR_H
#define	CURSOR_H


#include <OgrePanelOverlayElement.h>

#include "GUIImage.h"

class Cursor: public GUIImage{
    
    public:
        Cursor(const String& name, const String& texture): GUIImage(name, texture){
            
        }

};

#endif	/* CURSOR_H */

