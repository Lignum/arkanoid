/* 
 * File:   GUIGroup.h
 * Author: victor
 *
 * Created on 20 de febrero de 2015, 21:22
 */

#ifndef GUIGROUP_H
#define	GUIGROUP_H


#include <OgrePanelOverlayElement.h>
#include <vector>

#include "GUIImage.h"
#include "Cursor.h"
#include "Button.h"
#include "Text.h"
#include "TextField.h"
#include "Slider.h"

using namespace Ogre;

class GUIGroup{
    
    private:
        
        String _name;
        
        Overlay* _overlay;

        std::vector<GUIImage*> _GUIImages;
        std::vector<Button*> _buttons;
        std::vector<TextField*> _textFields;
        std::vector<Text*> _texts;
        std::vector<Slider*> _sliders;
        
        Real _wWidth;
        Real _wHeight;        
        
        Real _charHeight;
        String _textFont;
        ColourValue _textColor;
        
        void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
    public:
        GUIGroup(std::string name, Real width, Real height){
            _overlay = OverlayManager::getSingletonPtr()->create(name);
            _overlay->setZOrder(640);
            _overlay->show();
            
            _name = name;

            _charHeight = -1;
            _textFont = "";
            _textColor = ColourValue::ZERO;

            
            setDimensions(width, height);
        }
        
        const String getName(){
            return _name;
        }
        
        void update(){
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->update();
            }
        }
        
        void setAllvisible(bool visible){
            
            for (long unsigned int i=0; i<_GUIImages.size(); i++){
               _GUIImages[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_buttons.size(); i++){
               _buttons[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_texts.size(); i++){
               _texts[i]->setVisible(visible);
            }
                
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_sliders.size(); i++){
                _sliders[i]->setVisible(visible);
            }
        }
        
        void setDefaultTextCharHeight(Real height){
            
            _charHeight = height;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setCharHeight(height);
            }
        }
        
        void setDefaultTextFont(String font){
            
            _textFont = font;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setFont(_textFont);
            }
        }
        
        void setDefaultTextColor(ColourValue color){
            
            _textColor = color;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setColor(color);
            }
        }
        
        std::vector<Button*> getButtons(){
            return _buttons;
        }
        
        Button* getButton(const String& name){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if( _buttons[i]->getName() == name){
                    return _buttons[i];
                }
            }
            
            return NULL;
        }
        
        Text* getText(const String& name){
            for (long unsigned int i=0; i<_texts.size(); i++){
                if( _texts[i]->getName() == name){
                    return _texts[i];
                }
            }
            
            return NULL;
        }
        
        void keyPressed(const OIS::KeyEvent &e){
            if(e.key == OIS::KC_DOWN or e.key == OIS::KC_RIGHT){
                unsigned int num = getButtonFocusNum();
                unFocus();
                unsigned int n = 1;
                if((int)num == -1){
                    n = 0;
                    while(!_buttons[n]->isVisible()){
                        n++;
                    }
                    _buttons[n]->setFocus(true);
                }else{
                    if(num < _buttons.size()-1){
                        _buttons[num]->setFocus(false);
                        while(!_buttons[num+n]->isVisible()){
                            n++;
                            if(num+n == _buttons.size()){
                                num = 0;
                                n = 0;
                            }
                        }
                        _buttons[num+n]->setFocus(true);
                    }else{
                        n = 0;
                        _buttons[num]->setFocus(false);
                        while(!_buttons[n]->isVisible()){
                            n++;
                        }
                        _buttons[n]->setFocus(true);                        
                    }
                }
            }
            
            //UP
            if(e.key == OIS::KC_UP or e.key == OIS::KC_LEFT){
                int num = getButtonFocusNum();
                unFocus();
                int n = 1;
                if(num == -1){
                    num = _buttons.size();
                    while(!_buttons[num-n]->isVisible()){
                        n++;
                    }
                    _buttons[num-n]->setFocus(true);
                }else{
                    if(num == 0){
                        _buttons[0]->setFocus(false);
                        while(!_buttons[_buttons.size()-n]->isVisible()){
                            n++;
                        }
                        _buttons[_buttons.size()-n]->setFocus(true);
                    }else{
                        _buttons[num]->setFocus(false);
                        while(!_buttons[num-n]->isVisible()){
                            n++;
                            if(num-n == -1){
                                num = _buttons.size()-1;
                                n = 0;
                            }
                        }
                        _buttons[num-n]->setFocus(true);
                    }
                }
            }
            
            for (long unsigned int i=0; i<_textFields.size(); i++){
                    
                if(_textFields[i]->isSelected()){
                    
                    _textFields[i]->keyPressed((char)e.text);
                }

            }
        }
        
        void click(OIS::MouseButtonID id){

            if(id == OIS::MB_Left){
                for (long unsigned int i=0; i<_buttons.size(); i++){
                    if(_buttons[i]->isFocus()){
                        _buttons[i]->setClick(true);
                    }

                }

                for (long unsigned int i=0; i<_textFields.size(); i++){ 

                    _textFields[i]->setSelected(false);

                    if(_textFields[i]->isFocus()){
                        _textFields[i]->setSelected(true);
                    }

                }

                for (long unsigned int i=0; i<_sliders.size(); i++){
                    if(_sliders[i]->isFocus()){
                        _sliders[i]->setClick(true);
                    }
                }
                    
            }
        }
        
        int getButtonFocusNum(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if(_buttons[i]->isVisible()){
                    if(_buttons[i]->isFocus()){
                        return i;
                    }
                }
            }
            
            return -1;
        }
        
        void unFocus(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                _buttons[i]->setFocus(false);
            }
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setFocus(false);
                _textFields[i]->setSelected(false);
            }
        }
        
        void add(GUIImage* GUIElement){
            _GUIImages.push_back(GUIElement);
            _overlay->add2D(GUIElement->getContainer());
            GUIElement->setDimensions(_wWidth, _wHeight);
        }
        
        void add(Text* text){
            
            _texts.push_back(text);
            _overlay->add2D(text->getContainer());
                        
            text->setDimensions(_wWidth, _wHeight);
            
            if(_textFont != ""){
                text->setFont(_textFont);
            }
            
            if(_charHeight != -1){
                text->setCharHeight(_charHeight);
            }
            
            if(_textColor != ColourValue::ZERO){
                text->setColor(_textColor);
            }
            //GUIElement->setDimensions(_wWidth, _wH)eight);
        }
        
        void add(Button* button){
            _buttons.push_back(button);
            _overlay->add2D(button->getContainer());
            add(button->getText());
            button->setDimensions(_wWidth, _wHeight);
            button->centerText();
        }
        
        void add(TextField* field){
            
            _textFields.push_back(field);
            _overlay->add2D(field->getContainer());
            
            add(field->getText());
            field->setDimensions(_wWidth, _wHeight);
        }
        
        void add(Slider* slider){
            
            add(slider->getBar());
            _sliders.push_back(slider);
            _overlay->add2D(slider->getContainer());
            add(slider->getText());
            add(slider->getValText());
            slider->setDimensions(_wWidth, _wHeight);
        }
};

#endif	/* GUIGROUP_H */

