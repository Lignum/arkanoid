/* 
 * File:GUIImage.h
 * Author: victor
 *
 * Created on 11 de diciembre de 2014, 1:23
 */

#ifndef GUIImage_H
#define	GUIImage_H


#include "Element.h"
 
using namespace Ogre;

class GUIImage : public Element{
    
    protected:
        
        TexturePtr _texture;
        MaterialPtr _material;     
        

    public:
        GUIImage(const String& name, const String& texture): Element(name){

            _material = MaterialManager::getSingleton().create("GUIImage/"+name, "General");
            _container->setMaterialName(_material->getName());
            setImage(texture);
        }

        ~GUIImage(void) {}

        void setImage(const String& filename){
            _texture = TextureManager::getSingleton().load(filename, "General");

            TextureUnitState *pTexState;
            if(_material->getTechnique(0)->getPass(0)->getNumTextureUnitStates()){
                pTexState = _material->getTechnique(0)->getPass(0)->getTextureUnitState(0);
            } else {
                pTexState = _material->getTechnique(0)->getPass(0)->createTextureUnitState( _texture->getName() );
            }
            pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
            _material->setDiffuse(1,0,0,1);
            _material->getTechnique(0)->getPass(0)->setSceneBlending(SBT_TRANSPARENT_ALPHA);
        }
        
        void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;

            _container->setWidth(_texture->getWidth() / _wWidth);
            _container->setHeight(_texture->getHeight() / _wHeight);
        }
        
        void setColor(Real r, Real g, Real b, Real a){
            _material->setAmbient(r,g,b);
            _material->setDiffuse(r,g,b,a);
            /*std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa: " << _material->getTechnique(0)->getPass(0)->getDiffuse() << std::endl;
            _material->getTechnique(0)->getPass(0)->setDiffuse(r,g,b,a);
            std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa: " << _material->getTechnique(0)->getPass(0)->getDiffuse() << std::endl;
*/
        }

        
        TexturePtr getTexture(){
            return _texture;
        }
    
};

#endif	/* GUIImage_H */

