/* 
 * File:   Slider.h
 * Author: victor
 *
 * Created on 20 de febrero de 2015, 17:06
 */

#ifndef SLIDER_H
#define	SLIDER_H

#include "Button.h"


class Slider : public Button{
    
    private:
        GUIImage* _bar;
        Text* _val;
        
        int _max;
        int _min;
    
    public:
        Slider(const String& name, const String& texture, const String& focus, const String& click, const String& bar, const String& text) : Button(name, texture, focus, click, text){
            _bar = new GUIImage("bar/"+name, bar);
            
            _val = new Text("val/"+name, "0");
            
            _max = 100;
            _min = 0;            
        }
        
        GUIImage* getBar(){
            return _bar;
        }
        
        void setValPosition(Real x){
            
            
            int val = x-_bar->getX();
            
            if(x>=_bar->getX() && x<=_bar->getX()+_bar->getWidth()){
                GUIImage::setPosition(x,getY());
                val = x-_bar->getX();
            }else if(x<_bar->getX()){
                GUIImage::setPosition(_bar->getX(),getY());
                val = 0;       
            }else if(x>_bar->getX()+_bar->getWidth()){
                GUIImage::setPosition(_bar->getX()+_bar->getWidth(),getY());
                val = _bar->getWidth();         
            }
            
            val = ((val*(_max-_min))/_bar->getWidth())+_min;
            _val->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << val) )->str());
            
        }
        
        void setPosition(Real x, Real y){            
            
            int tX = _val->getX()-getX();
            int tY = _val->getY()-getY();
            
            Button::setPosition(x,y);
            _bar->setPosition(x,y);
            
            _val->setPosition(x+tX, y+tY);
        }
        void setVisible(bool visible){
            Button::setVisible(visible);
            
            _bar->setVisible(visible);
            _val->setVisible(visible);
        }
        
        void setValTextPosition(Real x, Real y){
            _val->setPosition(getX()+x, getY()+y);
        }
        
        void setMinMax(int min, int max){
            _max = max;
            _min = min;            
            
            setValPosition(_bar->getX());
        }
        
        Text* getValText(){
            return _val;
        }
        
        void setValAbs(int val){
            if(val<_min){
                val = _min;
            }
            if(val>_max){
                val = _max;
            }
            
            
        }
        void setValRel(Real val){
            
            if(val<0){
                val = 0;
            }
            if(val>1){
                val = 1;
            }
            
            _val->setText(StringConverter::toString((_max-_min)*val+_min));
            
            GUIImage::setPosition(_bar->getX()+(_bar->getWidth()*val),getY());
            
        }
        
        int getValAbs(){
            return StringConverter::parseInt(_val->getText());
        }
    
        Real getValRel(){ 
            Real val = getValAbs()-_min;
            return val/(_max-_min);
        }
};

#endif	/* SLIDER_H */

