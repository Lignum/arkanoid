/* 
 * File:   Console.h
 * Author: victor
 *
 * Created on 22 de diciembre de 2014, 14:07
 */

#ifndef CONSOLE_H
#define	CONSOLE_H

#include "GUIImage.h"
#include "Text.h"
#include <sstream> 

class Console{
    

    private:
        
        static Overlay* _overlay;
        
        static Real _wWidth;
        static Real _wHeight;
        
        static GUIImage* _background;
                
        static std::vector<Text*> _texts;
        
        static std::vector<Text*> _statics;
        
        Console(Viewport* viewport){
            
        }
        
        static void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
        static void initPosition(){
            setPosition(_wWidth - _background->getWidth(), 0);
        }
        
    public:
        
        
        
        static void init(Viewport* viewport){
            _overlay = OverlayManager::getSingletonPtr()->create("CONSOLE");
            _overlay->setZOrder(650);
            _overlay->show();
            
            setDimensions(viewport->getActualWidth(), viewport->getActualHeight());
            
            _background = new GUIImage("CONSOLE/GUIImage","Console.png");
            _overlay->add2D(_background->getContainer());            
            _background->setDimensions(_wWidth, _wHeight);
            
            Text *text = new Text("000CONSOLE/TEXT", "prueba", "Blue");
            text->setDimensions(_wWidth, _wHeight);
            
            //_texts.reserve(_background->getHeight()/text->getHeight());
            
            
            //_texts[0] = new Text("CONSOLE/TEXT", "prueba", "Blue");
            //_overlay->add2D(_texts[0]->getContainer());
            for (int i=0; i<18; i++){
                
                String s = static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();
                
                _texts.push_back(new Text(s+"CONSOLE/TEXT", "", "Blue"));
                _overlay->add2D(_texts[i]->getContainer());
                _texts[i]->setDimensions(_wWidth, _wHeight);
                _texts[i]->setCharHeight(0.023);
            }
            initPosition();
            
            _statics.reserve(10);
            
            
            log("INIT");

        }
        
        static void setPosition(int x, int y){            
            _background->setPosition(x,y);
                   

            for (long unsigned int i=0; i<_texts.size(); i++){
                _texts[i]->getContainer()->setPosition(x+3, y + (i*_texts[i]->getHeight()));
                _texts[i]->getContainer()->setDimensions(_background->getWidth() / _wWidth, _background->getHeight() / _wHeight);
            
            }
            
            if(_statics.size()>0){
                for (long unsigned int i=0; i<_statics.size(); i++){
                    
                    _statics[i]->getContainer()->setPosition(x-100, y + (i*_statics[i]->getHeight()));
                    _statics[i]->getContainer()->setDimensions(_background->getWidth() / _wWidth, _background->getHeight() / _wHeight);
            
                }
            }
           
        }
        
        template <typename T>
        static void setStatic(const String& name, T object){
            
            std::ostringstream stream;
            stream << object;
            
            bool find = false;
            
            if(_statics.size()>0){
                for (long unsigned int i=0; i<_statics.size(); i++){
                    
                    if(_statics[i]->getName() == name+"/CONSOLE/TEXT"){
                        
                        
                        _statics[i]->setText(stream.str().c_str());
                        find = true;
                        break;
                    }

                }
            }
            
            if(!find){
                
                Text* t = new Text(name+"/CONSOLE/TEXT", stream.str().c_str(), "Blue");
                _statics.push_back(t);            
                _overlay->add2D(_statics[_statics.size()-1]->getContainer());
                    
                t->setDimensions(_wWidth, _wHeight);
                t->setCharHeight(0.023);
                t->getContainer()->setPosition(_background->getX()-100, _background->getY() + (_statics.size()-1)*t->getHeight());
                t->getContainer()->setDimensions(_background->getWidth() / _wWidth, _background->getHeight() / _wHeight);
            
            }
        }

        /*static void setStatic(const String& name, const Real num){            
            Console::setStatic(name, Console::numToString(num));
        }
        */
        template <typename T>
        static void setStatic(const String& name, const String& text,  T object){     
            std::ostringstream stream;
            stream << object;       
            Console::setStatic(name, text+": " + stream.str().c_str());
        }
        
        template <typename T>
        static void log(T object){
            for (long unsigned int i=0; i<_texts.size()-1; i++){
                _texts[i]->setText(_texts[i+1]->getText());
                _texts[i]->setColor(_texts[i+1]->getColor());
            }
            
            std::ostringstream stream;
            stream << object;
            _texts[_texts.size()-1]->setText(stream.str().c_str());
            _texts[_texts.size()-1]->setColor(Ogre::ColourValue(1,1,1,1));
        }
        
        template <typename T>
        static void war(T object){
                        
            for (long unsigned int i=0; i<_texts.size()-1; i++){
                _texts[i]->setText(_texts[i+1]->getText());
                _texts[i]->setColor(_texts[i+1]->getColor());
            }
            
            std::ostringstream stream;
            stream << object;
            _texts[_texts.size()-1]->setText(stream.str().c_str());
            _texts[_texts.size()-1]->setColor(Ogre::ColourValue(1,1,0,1));
        }
        
        template <typename T>
        static void err(T object){
            for (long unsigned int i=0; i<_texts.size()-1; i++){
                _texts[i]->setText(_texts[i+1]->getText());
                _texts[i]->setColor(_texts[i+1]->getColor());
            }
            
            std::ostringstream stream;
            stream << object;
            _texts[_texts.size()-1]->setText(stream.str().c_str());
            _texts[_texts.size()-1]->setColor(Ogre::ColourValue(1,0,0,1));
        }
        
        static void setVisible(bool visible){
            if(visible){
                _overlay->show();
            }else{
                _overlay->hide();
            }
        }
        
        static bool isVisible(){
            return _overlay->isVisible();
        }
        

};

//
//        
//Overlay* Console::_overlay;        
//Real Console::_wWidth;
//Real Console::_wHeight;
//GUIImage* Console::_background;
//std::vector<Text*> Console::_texts;

#endif	/* CONSOLE_H */

