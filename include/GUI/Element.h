/* 
 * File:Element.h
 * Author: victor
 *
 * Created on 11 de diciembre de 2014, 1:23
 */

#ifndef Element_H
#define	Element_H

#include <OgrePanelOverlayElement.h>
 
using namespace Ogre;

class Element{
    
    protected:
        
        String _name;
        
        OverlayContainer* _container;

        
        Real _wWidth;
        Real _wHeight;
        
        

    public:
        Element(const String& name): _container(0){

            _name = name;
            
            _container = (OverlayContainer*) OverlayManager::getSingletonPtr()->createOverlayElement("Panel", "Element/"+name);
            _container->setPosition(0, 0);
            _container->show();

        }

        ~Element(void) {}

               
        void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
        bool isVisible(){
            return _container->isVisible();
        }
        
        void setVisible(bool visible){
            if(visible) {
                _container->show();
            } else {
                _container->hide();
            }
        }

        void setPosition(Real x, Real y){
            Real rx = x / _wWidth;
            Real ry = y / _wHeight;
            _container->setPosition(rx, ry);
        }
        
        Real getX(){
            
            Real x = _container->_getLeft() * _wWidth;
            return x;
        }
        
        Real getY(){
            
            Real y = _container->_getTop() * _wHeight;
            return y;
        }
        
        void setWidth(const int width){
            Real rx = width / _wWidth;
            _container->setWidth(clamp(rx, 0.0f, 1.0f));
        }
        
        void setHeight(const int height){
            Real ry = height / _wHeight;
            _container->setHeight(clamp(ry, 0.0f, 1.0f));
        }
        
        Real getWidth(){
            
            Real width = _container->_getWidth() * _wWidth;
            return width;
        }
        
        Real getHeight(){
            Real height = _container->_getHeight() * _wHeight;
            return height;
        }
                      
        const String getName(){
            return _name;
        }

        Real clamp(Real a, Real min, Real max){
            if (a < min) {
                return min;
            }
            if (a > max) {
                return max;
            } 
            return a;
        }
        
        OverlayContainer* getContainer(){
            return _container;
        }
};

#endif	/* Element_H */

