/* 
 * File:   GUI.h
 * Author: victor
 *
 * Created on 20 de enero de 2015, 15:30
 */

#ifndef GUI_H
#define	GUI_H

//GUI Container
    
    Overlay* GUIContainer::_overlay;
    Overlay* GUIContainer::_cursorOverlay;
    Cursor* GUIContainer::_cursor;

    std::vector<GUIImage*> GUIContainer::_GUIImages;
    std::vector<Button*> GUIContainer::_buttons;
    std::vector<TextField*> GUIContainer::_textFields;
    std::vector<Text*> GUIContainer::_texts;
    std::vector<Slider*> GUIContainer::_sliders;
    std::vector<GUIGroup*> GUIContainer::_groups;


    std::vector<Element*> GUIContainer::_elements;

    bool GUIContainer::_enable;
    bool GUIContainer::_firstFrame;

    Real GUIContainer::_wWidth;
    Real GUIContainer::_wHeight;


    Real GUIContainer::_charHeight;
    String GUIContainer::_textFont;
    ColourValue GUIContainer::_textColor;

    
//Console
    Overlay* Console::_overlay;        
    Real Console::_wWidth;
    Real Console::_wHeight;
    GUIImage* Console::_background;
    std::vector<Text*> Console::_texts;    
    std::vector<Text*> Console::_statics;

#endif	/* GUI_H */

