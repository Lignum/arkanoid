/* 
 * File:   CreateState.h
 * Author: victor
 *
 * Created on 18 de febrero de 2015, 16:39
 */

#ifndef CREATESTATE_H
#define	CREATESTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#include "GUI/GUIContainer.h"
#include "GUI/Cursor.h"
#include "GUI/GUIImage.h"
#include "GUI/Button.h"
#include "GUI/Text.h"

#include "Arkanoid/Score.h"

#include "Arkanoid/GameObjectManager.h"

#include "TrackManager.h"
#include "SoundFXManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class CreateState : public Ogre::Singleton<CreateState>, public GameState{
    
    private:
        
        //SOUND
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;
        //TrackPtr _mainTrack;
        SoundFXPtr _simpleEffect;
        
        bool _save;
        
        
        bool _double;        
        Real _r, _g, _b;
        
        GameObjectManager* _gom;
        
        void createGui();
        
        void menu();
        void save();
        void overSave();
        void cancel();
        
    public:

        CreateState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static CreateState& getSingleton ();
        static CreateState* getSingletonPtr ();

    protected:

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif	/* CREATESTATE_H */

