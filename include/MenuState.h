/* 
 * File:   MenuState.h
 * Author: victor
 *
 * Created on 9 de febrero de 2015, 15:54
 */

#ifndef MENUSTATE_H
#define	MENUSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#include "GUI/GUIContainer.h"
#include "GUI/Cursor.h"
#include "GUI/GUIImage.h"
#include "GUI/Button.h"
#include "GUI/Text.h"

#include "Arkanoid/Score.h"

#include "Arkanoid/GameObjectManager.h"

#include "TrackManager.h"
#include "SoundFXManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    
    private:
        
        //SOUND
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;
        //TrackPtr _mainTrack;
        SoundFXPtr _simpleEffect;
        
        void createGui();
        
        void play();
        void create();
        void closeGame();
        void showScore();
        void showCredits();
        
    public:

        MenuState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif	/* MENUSTATE_H */

