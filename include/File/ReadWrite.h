/* 
 * File:   ReadWrite.h
 * Author: victor
 *
 * Created on 4 de febrero de 2015, 16:06
 */

#ifndef READWRITE_H
#define	READWRITE_H

#include <iostream>
#include <fstream>

#include <stdio.h>

#include <Ogre.h>

using namespace std;

/*
 * 
 * 
 * NO FUNCIONA CON STRING USA CHAR[]
 * 
 * 
 *  */

template <class T>
class ReadWrite{
    
    private:
        string _file;
    
    public:
        
        ReadWrite(){
            
        }
        
        ReadWrite(string file) {
    
            _file = file;
            
        }
        
        void write(T data){
            
            write(data, false);
        }
        
        void write(T data, bool overwrote){
            
            std::vector<T > vec;
            
            if(!overwrote){
                vec = read();
            }
            
            vec.push_back(data);
            
            ofstream ofs(_file.c_str(), ios::binary);

            for(long unsigned int i=0; i<vec.size(); i++){                
                ofs.write((char *)&vec[i], sizeof(vec[i]));
            }

            ofs.close();
        }
        
        
        void write(std::vector<T > data){
            
            write(data, false);
        }
        
        void write( std::vector<T > data, bool overwrote){
            
            std::vector<T > vec;
            
            if(!overwrote){
                vec = read();
            }
            
            for(long unsigned int i=0; i<data.size(); i++){                
                vec.push_back(data[i]);
            }
            
            ofstream ofs(_file.c_str(), ios::binary);

            for(long unsigned int i=0; i<vec.size(); i++){                
                ofs.write((char *)&vec[i], sizeof(vec[i]));
            }

            ofs.close();
        }
        
        bool remove(){
            bool res = false;
            if(std::remove(_file.c_str()) == 0){
                res = true;
            }
            return res;
        }
        
        std::vector<T > read(){
            
            std::vector<T > data;
            T d;

            ifstream ifs(_file.c_str(),  ios::binary);
            while(ifs.read((char *)&d, sizeof(d))){
                data.push_back(d);
            }
            
            ifs.close();
            
            return data;
        }
    
};

#endif	/* READWRITE_H */

