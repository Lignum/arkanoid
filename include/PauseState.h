#ifndef PauseState_H
#define PauseState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"
#include "GUI/GUIContainer.h"
#include "GUI/Cursor.h"
#include "GUI/GUIImage.h"
#include "GUI/Button.h"
#include "GUI/Text.h"

#include "TrackManager.h"
#include "SoundFXManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

class PauseState : public Ogre::Singleton<PauseState>, public GameState{
    
    private:
        
        //SOUND
        SoundFXPtr _simpleEffect;
        
        void createGUI();
        
        void resumePlay();
        void menu();
        
    public:
        
        PauseState();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static PauseState& getSingleton ();
        static PauseState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif
