#include "Arkanoid/Base.h"
#include "Arkanoid/Cube.h"
#include "GUI/Console.h"

Base::Base(Ogre::SceneManager* sce, const String& name) : GameObject(sce, name, "Suelo.mesh") {
    init();
}

Base::Base(GameObjectManager* gom, const String& name) : GameObject(gom, name, "Suelo.mesh") {
    init();
}

Base::Base(GameObjectManager* gom, std::vector<Data::Point> data) : GameObject(gom, "Suelo.mesh") {
    
    _pSoundFXManager = SoundFXManager::getSingletonPtr();
    _wallCollison = _pSoundFXManager->load("MetalPing1.aiff");
    _boxCollison = _pSoundFXManager->load("MetalPing.aiff");
    
    init();
    
    for(long unsigned int i=0; i<data.size(); i++){
        createCube(data[i]._pos,data[i]._double, data[i]._color);
    }
    //_boxCollison = SoundFXManager::getSingletonPtr()->load("BoxCollision.wav");
    
}

Base::~Base() {;
}



void Base::init() {
    //Cube* c = new Cube(GameObject::getSceneManager(), 0,0,0,0,0);
        
    _CubeAnc = 0.75;
    _CubeAlt = -0.75;
    
    _walls.push_back(new GameObject(getSceneManager(), "ParedRight.mesh"));    
    _walls.push_back(new GameObject(getSceneManager(), "ParedLeft.mesh"));    
    _walls.push_back(new GameObject(getSceneManager(), "ParedUp.mesh"));
    //_walls.push_back(new GameObject(getSceneManager(), "ParedDown.mesh"));
    
    GameObject::addChild(_walls[0]);
    GameObject::addChild(_walls[1]);
    GameObject::addChild(_walls[2]);
    //GameObject::addChild(_walls[3]);
    
    setCastShadows(false, true);
    
    _paddle = new Paddle(_sceneMgr);
    GameObject::addChild(_paddle);
    _paddle->setVisible(false);
    
    _score = 0;
    _life = false;
    //delete c;    
    
    
}

void Base::update(Real delta) {
    
    _paddle->update(delta);
    
    for(long unsigned int i = 0; i<_balls.size(); i++){
        
        if(_balls[i]->getSticky() != -1000){
            _balls[i]->setPosition(_paddle->getPosition().x+_balls[i]->getSticky(), _paddle->getPosition().y);
        }
        
        if(_balls[i]->getSpeed()==0){
            _balls[i]->setPosition(_paddle->getPosition().x, _balls[i]->getPosition().y);
        }
        
        if(_balls[i]->getPosition().y < -23){
            _balls[i]->destroy();
            _balls.erase(_balls.begin()+i);
            
        }else{
            
            if(wallsCollision(delta, _balls[i])){
                _wallCollison->playChannel();
            }else{
                if(paddleCollision(delta, _balls[i])){                    
                    _boxCollison->playChannel();
                }else{
                    if(cubesCollision(delta, _balls[i])){
                        _boxCollison->playChannel();
                    }
                }
            }
            
            _balls[i]->update(delta);
        }
    }
    
    for(long unsigned int i = 0; i<_powerUps.size(); i++){
        _powerUps[i]->update(delta);
        
        if(_paddle->collide(_powerUps[i])){

            if(_powerUps[i]->getPosition().y > _paddle->getPosition().y - 0.5){
                
                switch(_powerUps[i]->getType()){
                    case PowerUp::T_DOUBLE:
                        duplicateBall();
                        break;
                    case PowerUp::T_LIFE:
                        _life = true;
                        break;
                    default:
                        _paddle->powerUp(_powerUps[i]->getType());
                        
                }
                
               _score += 20;
                
                _powerUps[i]->destroy();
                _powerUps.erase(_powerUps.begin()+i);
                
            }
        }
        
        if(_powerUps[i]->getPosition().y < -23){
            _powerUps[i]->destroy();
            _powerUps.erase(_powerUps.begin()+i);
            
        }
    }
    
}

bool Base::paddleCollision(Real delta, Ball* ball) {
    if(_paddle->collide(ball)){

        if(ball->getPosition().y > _paddle->getPosition().y - 0.5 && ball->getDirection().y < 0){
            
            ball->bounce(Vector2(0, 1));

            Real dis = ball->distance(_paddle)/2;

            if(ball->getPosition().x > _paddle->getPosition().x){
                ball->addDirection(Vector2(dis, 0));
            }else{
                ball->addDirection(Vector2(-dis, 0));
            }

            if(_paddle->isSticky()){
                
                ball->setSticky(ball->getPosition().x-_paddle->getPosition().x);
            }else{
                
                ball->translate(ball->getDirection()*/*ball->getSpeed()**/delta);
            }
            return true;
        }
    }
    
    return false;
}

bool Base::wallsCollision(Real delta, Ball* ball) {
        
    if((ball->getPosition().x < 0 && ball->getDirection().x<0) or (ball->getPosition().x > 17 && ball->getDirection().x>0) ){
        ball->bounce(Vector2(1, 0));
        ball->translate(ball->getDirection()*/*ball->getSpeed()**/delta);
        return true;
    }

    if(ball->getPosition().y > 0 && ball->getDirection().y>0){
        ball->bounce(Vector2(0, 1));
        ball->translate(ball->getDirection()*/*ball->getSpeed()**/delta);
        return true;
    }
    return false;
}

bool Base::cubesCollision(Real delta, Ball* ball) {
    
    bool col = false;
    
    for(long unsigned int e = 0; e<_cubes.size(); e++){
        
        col = false;
        
        switch(ball->collide(_cubes[e], false)){
            case 0:
                ball->bounce(Vector2(0, 1));
                col = true;
                break;
            case 1:
                ball->bounce(Vector2(0, 1));
                col = true;
                break;
            case 2:
                ball->bounce(Vector2(1, 0));
                col = true;
                break;
            case 3:
                ball->bounce(Vector2(1, 0));
                col = true;
                break;
        }
        
        if(col){
            
            ball->translate(ball->getDirection()*/*ball->getSpeed()**/delta);
            
            createPowerUp(_cubes[e]->getPosition().x, _cubes[e]->getPosition().y);
            
            _cubes[e]->destroy();
            _cubes.erase(_cubes.begin()+e);
            
            _score += 10;
            
            break;
        }

    }
    
    
    return col;
}

void Base::keyPressed(const OIS::KeyEvent& e) {
    _paddle->keyPressed(e);
    
    if(e.key == OIS::KC_SPACE){
        if(_balls[0]->getSpeed()<10){
            _balls[0]->start();
        }
        
        for(long unsigned int i = 0; i<_balls.size(); i++){
            if(_balls[i]->getSticky()!=-1000){
                _balls[i]->setSticky(-1000);
            }
        }
    }
    
    
    
}

void Base::keyReleased(const OIS::KeyEvent& e) {
    _paddle->keyReleased(e);
}

void Base::start() {
    
    
    //_boxCollison = _pSoundFXManager->load("BoxCollision.wav");
    
    _paddle->setVisible(true);
    
    startBall();
}

void Base::clear() {
    _balls.erase(_balls.begin(), _balls.end());
    _cubes.erase(_cubes.begin(), _cubes.end());
}

void Base::createCube(int x, int y, bool doubl, Real r, Real g, Real b) {
    
    bool res = true;
    
    for(unsigned int i=1; i<_cubes.size(); i++){
        if(_cubes[i]->getPosition().x == x*_CubeAnc && _cubes[i]->getPosition().y == y*_CubeAlt){            
            res = false;
            break;
        };
        if(_cubes[i]->getPosition().x == (x-1)*_CubeAnc && _cubes[i]->getPosition().y == y*_CubeAlt && _cubes[i]->isDouble()){
            res = false;
            break;
        };
        if(doubl && _cubes[i]->getPosition().x == (x+1)*_CubeAnc && _cubes[i]->getPosition().y == y*_CubeAlt){
            res = false;
            break;
        }
    }
    if(res){
        _cubes.push_back(new Cube(getSceneManager(), x*_CubeAnc, y*_CubeAlt, doubl, r,g,b));
        GameObject::addChild(_cubes[_cubes.size()-1]);
    }
}

void Base::createCube(Vector2 pos, bool doubl, Vector3 color) {
    createCube(pos.x, -pos.y, doubl, color.x, color.y, color.z);
}

void Base::setCubePosition(int cube, int x, int y) {
    _cubes[cube]->setPosition(x*_CubeAnc, y*_CubeAlt);
}

void Base::createPowerUp(int x, int y) {
    
    int r = rand() % 20;
    Console::log(r);
    if(r<1){
        _powerUps.push_back(new PowerUp(getSceneManager(), x+1, y));
        GameObject::addChild(_powerUps[_powerUps.size()-1]);        
    }
}

void Base::startBall() {
    createBall(_paddle->getPosition().x, _paddle->getPosition().y+0.5);
}

void Base::createBall(Real x, Real y) {
    _balls.push_back(new Ball(getSceneManager(), x, y));
    GameObject::addChild(_balls[_balls.size()-1]);
}

void Base::duplicateBall(){
    
    std::vector<Ball*> vec;

    for(unsigned int i = 0; i<_balls.size(); i++){
        vec.push_back(new Ball(getSceneManager(), _balls[i]->getPosition().x, _balls[i]->getPosition().y));
        vec[vec.size()-1]->start(_balls[i]->getSpeed(),_balls[i]->getDirection()+Vector2(0.5,0));
        vec[vec.size()-1]->setSticky(_balls[i]->getSticky());
        
        vec.push_back(new Ball(getSceneManager(), _balls[i]->getPosition().x, _balls[i]->getPosition().y));
        vec[vec.size()-1]->start(_balls[i]->getSpeed(),_balls[i]->getDirection()+Vector2(-0.5,0));
        vec[vec.size()-1]->setSticky(_balls[i]->getSticky());
        
        //createBall(_balls[i]->getPosition().x, _balls[i]->getPosition().y);
        if(i>100){
            break;
        }
    }
    
    
    for(unsigned int i = 0; i<vec.size(); i++){
        _balls.push_back(vec[i]);
        GameObject::addChild(vec[i]);
        if(i>100){
            break;
        }
    }
}

bool Base::hasLife() {
    if(_life){
        _life = false;
        return true;
    }
    return false;
}

void Base::setCastShadows(bool cast, bool cubes) {
    
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->setCastShadows(cast);
    
    for(unsigned int i=0; i<_cubes.size(); i++){
        _cubes[i]->setCastShadows(cubes);
    }
    for(long unsigned int e = 0; e<_walls.size(); e++){
        _walls[e]->setCastShadows(cubes);
    }
    
}

int Base::getBallsNum() {
    return _balls.size();
}

int Base::getCubesNum() {
    return _cubes.size();
}

int Base::getScore() {
    return _score;
}

void Base::destroyCube(int n) {
    _cubes[n]->destroy();
    _cubes.erase(_cubes.begin()+n);
}

void Base::destroyCube(int x, int y) {
    for(unsigned int i=1; i<_cubes.size(); i++){
        if(_cubes[i]->getPosition().x == x*_CubeAnc && _cubes[i]->getPosition().y == y*_CubeAlt){
            
            _cubes[i]->destroy();
            _cubes.erase(_cubes.begin()+i);
            
            break;
        }
    }
}

Cube* Base::getCube(int x, int y) {

    for(unsigned int i=1; i<_cubes.size(); i++){
        if(_cubes[i]->getPosition().x == x*_CubeAnc && _cubes[i]->getPosition().y == y*_CubeAlt){
            return _cubes[i];
            break;
        }
    }
    return NULL;
}


std::vector<Data::Point> Base::getCubesData() {
    std::vector<Data::Point> d;
    for(long unsigned int i=0; i<_cubes.size(); i++){        
        d.push_back(Data::Point(_cubes[i]));
    }
    
    
    return d;
}

std::vector<Cube*> Base::getCubes() {
    return _cubes;
}

std::vector<Ball*> Base::getBalls() {
    return _balls;
}