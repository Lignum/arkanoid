#include "Arkanoid/Cube.h"
#include "GUI/Console.h"

Cube::Cube(SceneManager* sce, Real x, Real y, bool doubl, Real r, Real g, Real b) : GameObject(sce, "Box.mesh", x, y, 0) {
    GameObject::setColor(r,g,b,1);
    setDouble(doubl);
}

Cube::Cube(GameObjectManager* gom, Real x, Real y, bool doubl, Real r, Real g, Real b) : GameObject(gom, "Box.mesh", x, y, 0) {
    GameObject::setColor(r,g,b,1);
    setDouble(doubl);
}

Cube::~Cube() {
}

void Cube::update(Real delta) {

}


void Cube::setPosition(Real x, Real y) {
    GameObject::setPosition(x, y, 0);
}

void Cube::setDouble(bool dou) {
    if(dou){
        setScale(3,1.5,1.5);
    }else{
        setScale(1.5,1.5,1.5);
    }
}

bool Cube::isDouble() {
    if(getScale().x > 1.5){
        return true;
    }else{
        return false;
    }
}



