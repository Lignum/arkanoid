#include "PauseState.h"
#include "GUI/GUIContainer.h"
#include "PlayState.h"
#include "MenuState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

PauseState::PauseState() {

}

void PauseState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.

    _exitGame = false;

    _simpleEffect = SoundFXManager::getSingletonPtr()->load("MetalPing.aiff");
    
    createGUI();
}

void PauseState::createGUI() {
    GUIContainer::showCursor(true);
    
    GUIContainer::createImage("imgBlackBox","blackBox.png");
    GUIContainer::getGUIImage("imgBlackBox")->setPosition(683-400,384-200);

    GUIContainer::createText("txtPause","PAUSE");
    GUIContainer::getText("txtPause")->setPosition(683, 250);
    GUIContainer::getText("txtPause")->setCharHeight(0.05);
    GUIContainer::getText("txtPause")->setAlignment(TextAreaOverlayElement::Center);

    GUIContainer::createButton("btnResume", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "RESUME");
    GUIContainer::getButton("btnResume")->setFocus(true);
    GUIContainer::getButton("btnResume")->setPosition(500,400);

    GUIContainer::createButton("btnMenu", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "EXIT");
    GUIContainer::getButton("btnMenu")->setPosition(700,400);
}


void PauseState::exit (){
}

void PauseState::pause (){
}

void PauseState::resume (){
}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt){
    return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt){
    if (_exitGame)
        return false;

    return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
    // Tecla p o ESCAPE --> Resume
    if (e.key == OIS::KC_P or e.key == OIS::KC_ESCAPE) {
        resumePlay();
    }
    
    GUIContainer::keyPressed(e);
    if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        GUIContainer::getButton("btnResume")->enter(&PauseState::resumePlay, *this);
        GUIContainer::getButton("btnMenu")->enter(&PauseState::menu, *this);
    }
    
    if (e.key == OIS::KC_I) {
        changeState(MenuState::getSingletonPtr());
    }
    
}

void PauseState::keyReleased (const OIS::KeyEvent &e){
    
    
    
    if (e.key == OIS::KC_Y) {
        _exitGame = true;
    }
}

void PauseState::mouseMoved (const OIS::MouseEvent &e){
    GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(GUIContainer::cursorIsVisible()){
        GUIContainer::click(id);
    }
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(id == OIS::MB_Left && GUIContainer::cursorIsVisible()){
        GUIContainer::getButton("btnResume")->click(&PauseState::resumePlay, *this);
        GUIContainer::getButton("btnMenu")->click(&PauseState::menu, *this);
    }
}

void PauseState::resumePlay() {
    _simpleEffect->play();
    GUIContainer::setAllvisible(false);
    popState();
}

void PauseState::menu() {
    _simpleEffect->play();
    GUIContainer::setAllvisible(false);
    PlayState::getSingletonPtr()->_gom->clear();
    changeState(MenuState::getSingletonPtr());
}

PauseState* PauseState::getSingletonPtr (){
    return msSingleton;
}

PauseState& PauseState::getSingleton (){ 
  assert(msSingleton);
  return *msSingleton;
}
