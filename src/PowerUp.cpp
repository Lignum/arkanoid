#include "Arkanoid/PowerUp.h"
#include "Arkanoid/GameObject.h"

//int PowerUp::T_LONG;

PowerUp::PowerUp(SceneManager* sce, Real x, Real y) : GameObject(sce, "power.mesh", x, y, 0){
    
    int r = rand() % 101;
    
    if(r<5){
        _type = T_LIFE;
    }else if(r<20){
        _type = T_STICKY;
    }else if(r< 40){
        _type = T_DOUBLE;
    }else{        
        _type = T_LONG;
    }
    
    //_type = rand() % 4;
    
    switch(_type){
        case T_LONG:
            setTexture("PowerUpLong.png");
            break;
        case T_DOUBLE:
            setTexture("PowerUpDouble.png");
            break;
        case T_LIFE:
            setTexture("PowerUpLife.png");
            break;
        case T_STICKY:
            setTexture("PowerUpSticky.png");
            break;
    }
}

void PowerUp::update(Real delta) {
    translate(0, -_speed*delta, 0);
}

int PowerUp::getType() {
    return _type;
}
