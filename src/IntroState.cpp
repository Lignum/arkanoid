#include "IntroState.h"
#include "PlayState.h"
#include "Arkanoid/GameObject.h"
#include "Arkanoid/GameObjectManager.h"
#include "Arkanoid/Base.h"
#include "Arkanoid/Cube.h"
#include "MenuState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState() {
    
}

void IntroState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _camera = _sceneMgr->createCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
        
    
    initSDL();
    
    
    
    //_viewport->setBackgroundColour(Ogre::ColourValue(0.5, 0.0, 0.0));
    
   // OIS::MouseState->width = _viewport->getWidth();
    
    
    _camera->setNearClipDistance(0.1f); // 
    _camera->setFarClipDistance(3000.0f);
    _camera->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));
    
    _exitGame = false;
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    Console::init(_viewport);
    Console::setVisible(false);
    _sceneMgr->showBoundingBoxes(false);
    

    
    GUIContainer::init(_viewport);
    //GUIContainer::addCursor(new Cursor("Cursor", "cursor.png"));
    GUIContainer::createCursor("cursor.png");
  
    
    
    
    _pTrackManager = new TrackManager;
    _pSoundFXManager = new SoundFXManager;
  /*
   *######################################
   * GUI
   *######################################
   */
    
    
    //createGui();
    
    
    
}

void IntroState::createGui() {
    
    GUIContainer::init(_viewport);

   // _gui = new GUIContainer("Base", _viewport);
    
    GUIContainer::createButton("btnPlay", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "PLAY");
    GUIContainer::createButton("btnScore", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "SCORE");
    GUIContainer::createButton("btnCredits", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "CREDITS");
    GUIContainer::createButton("btnExit", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "EXIT");
    
    GUIContainer::getButton("btnPlay")->setPosition(100, 100);   
    GUIContainer::getButton("btnScore")->setPosition(100, 200);
    GUIContainer::getButton("btnCredits")->setPosition(100, 300);
    GUIContainer::getButton("btnExit")->setPosition(100, 400);

    GUIContainer::addCursor(new Cursor("Cursor", "cursor.png"));

    GUIContainer::setDefaultTextCharHeight(0.03);
    GUIContainer::setDefaultTextFont("Blue");
    
    GUIContainer::getButton("btnPlay")->setFocus(true);
}


void IntroState::exit(){
    //_sceneMgr->clearScene();
    //_root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause (){
}

void IntroState::resume (){
    
    
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt){
    Console::setStatic("FPS", "FPS" ,1.0 / evt.timeSinceLastFrame);
    return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt){
    
    
    changeState(MenuState::getSingletonPtr());
    
    if (_exitGame)
        return false;
    
    return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e){
    // Transición al siguiente estado.
    // Espacio --> PlayState
    
    GUIContainer::keyPressed(e);
    if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        GUIContainer::getButton("btnPlay")->enter(&IntroState::play, *this);
        
        GUIContainer::getButton("btnExit")->enter(&IntroState::closeGame, *this);
    }
      
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    
    if (e.text == 186) {
        Console::setVisible(!Console::isVisible());
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
    }
}

void IntroState::keyReleased(const OIS::KeyEvent &e ){
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void IntroState::mouseMoved(const OIS::MouseEvent &e){
   
    GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
    
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
    GUIContainer::click(id);
}

void IntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(id == OIS::MB_Left){
        GUIContainer::getButton("btnPlay")->click(&IntroState::play, *this);
        
        GUIContainer::getButton("btnExit")->click(&IntroState::closeGame, *this);
    }
}

IntroState* IntroState::getSingletonPtr (){
    return msSingleton;
}

IntroState& IntroState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::play() {
    
    //_sceneMgr->clearScene();
    //_root->getAutoCreatedWindow()->removeAllViewports();
    //GUIContainer::setVisible(false);
    
    GUIContainer::setAllvisible(false);
    
    changeState(MenuState::getSingletonPtr());
}

void IntroState::closeGame() {
    _exitGame = true;
}


bool IntroState::initSDL () {
    // Inicializando SDL...
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
        return false;
    // Llamar a  SDL_Quit al terminar.
    atexit(SDL_Quit);
 
    // Inicializando SDL mixer...
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0)
      return false;
 
    // Llamar a Mix_CloseAudio al terminar.
    atexit(Mix_CloseAudio);
 
    return true;    
}