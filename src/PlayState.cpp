
#include "MenuState.h"

#include "PlayState.h"
#include "PauseState.h"
#include "GUI/Console.h"

#include "Arkanoid/GameObject.h"
#include "Arkanoid/Base.h"
#include "Arkanoid/Cube.h"
#include "Arkanoid/Data.h"
#include "IntroState.h"
#include "Arkanoid/Score.h"




template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

PlayState::PlayState() {

}


void PlayState::enter (){
    
    
    _root = Ogre::Root::getSingletonPtr();
    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _camera->setPosition(0,0,0);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
    _exitGame = false;
    
    Console::log("PLAY STATE -- ENTER");
    
    _stage = 0;
    _selectState = true;
    _playState = false;
    _gameOver = false;
    _newStage = false;
    
    _ballsNum = 3;
    _round = 1;
    
    ReadWrite<Score> rw("score.sav");
    _highScore = rw.read()[0].score;
    
    
    _simpleEffect = SoundFXManager::getSingletonPtr()->load("MetalPing.aiff");
    
    /* 
     * #########################################################################
     * LUCES
     * #########################################################################
     */
    
    
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
    //_sceneMgr->setShadowFarDistance(22);
    // _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);	
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    
    _sceneMgr->setShadowTextureCount(2);
    _sceneMgr->setShadowTextureSize(1024);
    //_sceneMgr->setShadowTextureSelfShadow(true);

    
    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setPosition(-11, 10,-10);
    light->setDiffuseColour(0.4, 0.2, 0.2);
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(1,-1,-2));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);

    Ogre::Light* light2 = _sceneMgr->createLight("Light2");
    light2->setPosition(7, 10, -10);
    light2->setDiffuseColour(0.2, 0.4, 0.2);
    light2->setType(Ogre::Light::LT_DIRECTIONAL);
    light2->setDirection(Ogre::Vector3(-1,-1,-2));
    light2->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light2->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light2->setSpotlightFalloff(5.0f);
    light2->setCastShadows(true);
    
    
    /* 
     * #########################################################################
     * OBJETOS
     * #########################################################################
     */
      
    

    _gom = new GameObjectManager(_sceneMgr);
    GUIContainer::showCursor(false);
    
    loadStages();
    
   
}

void PlayState::loadStages() {        
    
    Data* d = new Data();
    //d->addStage(static_cast<Base*>(_gom->getObjectByName("base"))->getCubesData());
    //std::vector<std::string> ss = d->getNames();
    //std::vector<Data::Point> dada = d->getStage("Prueba");
    
    std::vector<std::vector<Data::Point> > stages = d->getAllStages();
    
    GUIContainer::createGroup("playState");
    
    for(long unsigned int i=0; i< stages.size(); i++){
        new Base(_gom, stages[i]);
        
    }
    //CreateGui();
    for(long unsigned int i=0; i< d->getNames().size(); i++){        
        GUIContainer::createButton("playState",d->getNames()[i], "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", d->getNames()[i]);
    }
    GUIContainer::getButton("playState",d->getNames()[_stage])->setFocus(true);
}

void PlayState::loadStage(int num) {
    //_gom->clear();
    Data* d = new Data();
    if(num>=(int)d->getNames().size()){
        num = 0;
    }
    new Base(_gom, d->getAllStages()[num]);
    GUIContainer::getText("stage")->setText(d->getNames()[num]);
    
    _gom->getObject(1)->setPosition(23,8,-30);
    //static_cast<Base*>(_gom->getObject(0))->start();
}

void PlayState::CreateGui() {
    //GUIContainer::showCursor(false);
    _round = 1;
    GUIContainer::createImage("puntuation","puntuation.png");
    
    GUIContainer::createText("score","0");    
    GUIContainer::createText("highScore",static_cast<std::ostringstream*>( &(std::ostringstream() << _highScore) )->str());
    GUIContainer::createText("round",static_cast<std::ostringstream*>( &(std::ostringstream() << _round) )->str());
    GUIContainer::createText("balls",static_cast<std::ostringstream*>( &(std::ostringstream() << _ballsNum) )->str());
    
    Data* d = new Data();
    GUIContainer::createText("stage",d->getNames()[_stage]);
    
    
    GUIContainer::getText("round")->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << _round) )->str());
    
    GUIContainer::getText("stage")->setPosition(180,70);
    GUIContainer::getText("score")->setPosition(300,240);
    GUIContainer::getText("highScore")->setPosition(300,370);
    GUIContainer::getText("round")->setPosition(300,495);
    GUIContainer::getText("balls")->setPosition(300,630);
    
    GUIContainer::getText("stage")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("score")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("highScore")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("round")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("balls")->setAlignment(TextAreaOverlayElement::Right);
    
    GUIContainer::getText("stage")->setColor(ColourValue::Black);
    GUIContainer::getText("score")->setColor(ColourValue::Black);
    GUIContainer::getText("highScore")->setColor(ColourValue::Black);
    GUIContainer::getText("round")->setColor(ColourValue::Black);
    GUIContainer::getText("balls")->setColor(ColourValue::Black);
    
    GUIContainer::getText("stage")->setCharHeight(0.1);
    GUIContainer::getText("score")->setCharHeight(0.1);
    GUIContainer::getText("highScore")->setCharHeight(0.1);
    GUIContainer::getText("round")->setCharHeight(0.1);
    GUIContainer::getText("balls")->setCharHeight(0.1);
    //GUIContainer::createImage("Nombre","Button1.png");
}

void PlayState::resetPoint() {
    
    Data* d = new Data();
    GUIContainer::getText("stage")->setText(d->getNames()[_stage]);
    GUIContainer::getText("score")->setText(StringConverter::toString(_score));
    GUIContainer::getText("highScore")->setText(StringConverter::toString(_highScore));
    GUIContainer::getText("round")->setText(StringConverter::toString(_round));
    GUIContainer::getText("balls")->setText(StringConverter::toString(_ballsNum));
}


void PlayState::exit (){
    
    _ballsNum = 3;
    _score = 0;
    _totalScore = 0;
    _round = 1;
    
    resetPoint();
    //_sceneMgr->clearScene();
    //_root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause(){
}

void PlayState::resume(){
    Console::log("PLAY STATE -- RESUME");
    CreateGui();
}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt){
    
    Console::setStatic("FPS", "FPS" ,1.0 / evt.timeSinceLastFrame);
    _camera->lookAt(0,0,-40);
    
    if(_selectState){
        selectState();
        
    }else{
        if(_playState){
            playState(evt);
        }else{
            
            if(_newStage){
                newStage(evt);
            }else{
                if(!_gameOver){
                    _camera->move(Vector3(0,-0.1,-0.03)); //-12 -3.6
                    if(_camera->getPosition().y<=-12) _playState = true;
                }     
            }
        }
    }
    
   
    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt){
    
    GUIContainer::update();
    
    if (_exitGame)
        return false;
    
    return true;
}

void PlayState::selectState() {
    unsigned int focus = GUIContainer::getGroup("playState")->getButtonFocusNum();
    int pos = 0;
    //Base* b;

    for(unsigned int i=0; i<GUIContainer::getGroup("playState")->getButtons().size(); i++){
        if(GUIContainer::getGroup("playState")->getButtons()[i]->isVisible()){

            pos = 0;


            GUIContainer::getGroup("playState")->getButtons()[i]->setHeight(50);
            GUIContainer::getGroup("playState")->getButtons()[i]->setWidth(150);

            //b = static_cast<Base*>(_gom->getObject(i-4));
            //b->setCastShadows(false, false);


            if(i<focus){
                GUIContainer::getGroup("playState")->getButtons()[i]->setPosition(600-(150*(focus-i)),0);
                pos = -22-20*(focus-i);
                _gom->getObject(i)->setPosition(pos,8,-80);
            }
            if(i == focus){
                GUIContainer::getGroup("playState")->getButtons()[i]->setPosition(600,0);
                GUIContainer::getGroup("playState")->getButtons()[i]->setHeight(70);                    
                GUIContainer::getGroup("playState")->getButtons()[i]->setWidth(200);

                _gom->getObject(i)->setPosition(-8,8,-30);
                //b->setCastShadows(false, true);

                _stage = i;
            }
            if(i>focus){

                pos = 8+20*(i-focus);
                _gom->getObject(i)->setPosition(pos,8,-80);
                if(i == focus+1){                
                    GUIContainer::getGroup("playState")->getButtons()[i]->setPosition(600+(200*(i-focus)),0);
                }else{
                    GUIContainer::getGroup("playState")->getButtons()[i]->setPosition(650+(150*(i-focus)),0);
                }
            }


            GUIContainer::getGroup("playState")->getButtons()[i]->centerText();
        }
    }
}

void PlayState::playState(const Ogre::FrameEvent& evt) {
    _gom->update(evt.timeSinceLastFrame);
    
    _score = static_cast<Base*>(_gom->getObject(0))->getScore()*(1+(0.1*(_round-1)));
    GUIContainer::getText("score")->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << _totalScore+_score) )->str());
    
    if(static_cast<Base*>(_gom->getObject(0))->hasLife()){
        _ballsNum++;
        GUIContainer::getText("balls")->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << _ballsNum) )->str());
    }
    
    
    if(static_cast<Base*>(_gom->getObject(0))->getCubesNum() <= 0){
        
        _stage++;
        loadStage(_stage);
         
        _round++;
        GUIContainer::getText("round")->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << _round) )->str());
        
        _totalScore += _score;
        
        _playState = false;
        _newStage = true;
        
    }else{
        if(static_cast<Base*>(_gom->getObject(0))->getBallsNum() <= 0){
            _ballsNum--;
            if(_ballsNum!=0){
                static_cast<Base*>(_gom->getObject(0))->startBall();
            }else{
                _totalScore += _score;
            }
            GUIContainer::getText("balls")->setText(static_cast<std::ostringstream*>( &(std::ostringstream() << _ballsNum) )->str());

        }
        
        if(_ballsNum <= 0){
            
            GUIContainer::createImage("imgBlackBox","blackBox.png");
            GUIContainer::getGUIImage("imgBlackBox")->setPosition(683-400,384-200);
            
            GUIContainer::createTextField("save", "TextFieldBase.png", "NAME");
            GUIContainer::getTextField("save")->getText()->setText("");
            GUIContainer::getTextField("save")->setPosition(500, 300);
            GUIContainer::getTextField("save")->setTextPosition(18,18);
            GUIContainer::getTextField("save")->getText()->setCharHeight(0.025);
            GUIContainer::getTextField("save")->setOnlyUpper(true);
            GUIContainer::getTextField("save")->setMaxChar(20);
            GUIContainer::getTextField("save")->setSelected(true);
            
            GUIContainer::createButton("btnOverSave", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "SAVE");
            GUIContainer::getButton("btnOverSave")->setPosition(700,300);
            GUIContainer::getButton("btnOverSave")->setHeight(50);
            GUIContainer::getButton("btnOverSave")->centerText();

            GUIContainer::createText("txtGameOver","GAME OVER");
            GUIContainer::getText("txtGameOver")->setPosition(683, 250);
            GUIContainer::getText("txtGameOver")->setCharHeight(0.05);
            GUIContainer::getText("txtGameOver")->setAlignment(TextAreaOverlayElement::Center);

            GUIContainer::createButton("btnOverRetry", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "RETRY");
            //GUIContainer::getButton("btnOverRetry")->setFocus(true);
            GUIContainer::getButton("btnOverRetry")->setPosition(500,400);

            GUIContainer::createButton("btnOverMenu", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "EXIT");
            GUIContainer::getButton("btnOverMenu")->setPosition(700,400);

           
            
            
            
            GUIContainer::showCursor(true);
            _playState = false;
            _gameOver = true;
        }
            
    }
    
    
}

void PlayState::newStage(const Ogre::FrameEvent& evt) {
    _gom->getObject(0)->translate(-4*evt.timeSinceLastFrame,0,0);
    _gom->getObject(1)->translate(-4*evt.timeSinceLastFrame,0,0);
    
    if(_gom->getObject(1)->getPosition().x <= -8){
        _playState = true;
        _newStage = false;
        
        static_cast<Base*>(_gom->getObject(0))->clear();
        _gom->destroyObject(0);
        static_cast<Base*>(_gom->getObject(0))->start();
    }
}

void PlayState::keyPressed(const OIS::KeyEvent &e){
    if(_selectState){
    
        //GUIContainer::keyPressed(e);
        GUIContainer::getGroup("playState")->keyPressed(e);
        
        //ENTER o SPACE-> start
        if (e.key == OIS::KC_RETURN or e.key == OIS::KC_SPACE) {
            
            GUIContainer::setAllvisible(false);
            
            _gom->destroyObject(_stage+1,_gom->getObjects().size());
            _gom->destroyObject(0,_stage);
            
            static_cast<Base*>(_gom->getObject(0))->start();
            
            _selectState = false;            
            
            CreateGui();
            resetPoint();
            
        }
        
        //ESC -> menu
        if (e.key == OIS::KC_ESCAPE) {
            overExit();
            /*GUIContainer::setAllvisible(false);
            _gom->clear();
            pushState(MenuState::getSingletonPtr());*/
        }
    }else{
    
        if(_playState){
            _gom->keyPressed(e);

            // Tecla P o ESCAPE --> PauseState.
            if (e.key == OIS::KC_P or e.key == OIS::KC_ESCAPE) {        
                pushState(PauseState::getSingletonPtr());
            }
        }
    }
    
    if(_gameOver){
        GUIContainer::keyPressed(e);
        if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
            GUIContainer::getButton("btnOverRetry")->enter(&PlayState::overRetry, *this);
            GUIContainer::getButton("btnOverMenu")->enter(&PlayState::overExit, *this);
            GUIContainer::getButton("btnOverSave")->enter(&PlayState::overSave, *this);
        }
    }
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    
    if (e.text == 186) {
        Console::setVisible(!Console::isVisible());
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
    }
}

void PlayState::keyReleased(const OIS::KeyEvent &e){
    
    _gom->keyReleased(e);
   
}

void PlayState::mouseMoved(const OIS::MouseEvent &e){
        
    if(_gameOver){
        GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
    }
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(GUIContainer::cursorIsVisible()){
        GUIContainer::click(id);
    }
    
    
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(id == OIS::MB_Left && GUIContainer::cursorIsVisible()){
        GUIContainer::getButton("btnOverRetry")->click(&PlayState::overRetry, *this);
        GUIContainer::getButton("btnOverMenu")->click(&PlayState::overExit, *this);
        GUIContainer::getButton("btnOverSave")->enter(&PlayState::overSave, *this);
    }
}

PlayState* PlayState::getSingletonPtr (){
    return msSingleton;
}

PlayState& PlayState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::overRetry() {
    
    _simpleEffect->play();
    
    GUIContainer::setAllvisible(false);
    _gom->clear();
    loadStages();
    
    _ballsNum = 3;
    
    _totalScore = 0;
    _round = 1;
    _score = 0;
    
    resetPoint();
    
    _camera->setPosition(0,0,0);
    
    _selectState = true;
    _playState = false;
    _gameOver = false;
}

void PlayState::overExit() {
    
    _simpleEffect->play();   
    
    _ballsNum = 3;
    
    _totalScore = 0;
    _round = 1;
    _score = 0;
    
    resetPoint();
    
    GUIContainer::setAllvisible(false);
    _gom->clear();
    _sceneMgr->clearScene();
    changeState(MenuState::getSingletonPtr());
    
}

void PlayState::overSave() {
    
    _simpleEffect->play();
    
    ReadWrite<Score> rw("score.sav");
    
    char c[20];
    
    for(int i=0; i<20; i++){
        c[i] = GUIContainer::getTextField("save")->getText()->getText()[i];
    }
    
    Score s(c,_totalScore);
    
    std::vector<Score> list = rw.read();
    list.push_back(s);
    std::sort(list.begin(), list.end());
    std::reverse(list.begin(), list.end());
    
    list.erase(list.begin()+30, list.end());
    
    rw.write(list, true);
    
    overRetry();
}