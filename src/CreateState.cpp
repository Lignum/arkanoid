#include "CreateState.h"
#include "GUI/Console.h"

#include "Arkanoid/GameObject.h"
#include "Arkanoid/Base.h"
#include "Arkanoid/Cube.h"
#include "Arkanoid/Data.h"
#include "IntroState.h"
#include "PauseState.h"
#include "PlayState.h"
#include "MenuState.h"



template<> CreateState* Ogre::Singleton<CreateState>::msSingleton = 0;

void CreateState::enter (){
    
    _root = Ogre::Root::getSingletonPtr();
    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    
    //_viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _exitGame = false;
    
    _double = false;
    
    _camera->setPosition(0,0,0);
    _camera->lookAt(0,0,-40);
    
    createGui();
    
    _pTrackManager = TrackManager::getSingletonPtr();
    _pSoundFXManager = SoundFXManager::getSingletonPtr();
            
    //_mainTrack = _pTrackManager->load("137290__fran-ky__126-numerology-metal-3-c.wav");
    
    //_mainTrack->play();
    _simpleEffect = _pSoundFXManager->load("MetalPing.aiff");
    
    
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    
    _sceneMgr->setShadowTextureCount(2);
    _sceneMgr->setShadowTextureSize(1024);
    
    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setPosition(-11, 12,-10);
    light->setDiffuseColour(0.4, 0.2, 0.2);
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(1,-1,-2));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);

    Ogre::Light* light2 = _sceneMgr->createLight("Light2");
    light2->setPosition(7, 12, -10);
    light2->setDiffuseColour(0.2, 0.4, 0.2);
    light2->setType(Ogre::Light::LT_DIRECTIONAL);
    light2->setDirection(Ogre::Vector3(-1,-1,-2));
    light2->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light2->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light2->setSpotlightFalloff(5.0f);
    light2->setCastShadows(true);
    
    _gom = new GameObjectManager(_sceneMgr);
    _gom->addObject(new Base(_sceneMgr, "create"));
    _gom->getObject(0)->setPosition(-8,10,-30);
    
    _save = false;
    
    _r = 1;
    _g = 1;
    _b = 1;
    
    static_cast<Base*>(_gom->getObject(0))->createCube(0,0, false, _r, _g, _b);
}

void CreateState::createGui() {
    
    GUIContainer::showCursor(true);
    
    GUIContainer::createButton("btnSimple", "btnSimple.png", "btnSimpleFocus.png", "btnSimpleClick.png", "");    
    GUIContainer::createButton("btnDouble", "btnDouble.png", "btnDoubleFocus.png", "btnDoubleClick.png", "");
    
    GUIContainer::getButton("btnSimple")->setPosition(1070,260);
    GUIContainer::getButton("btnDouble")->setPosition(1150,260);
    
    GUIContainer::createImage("imgSimple","btnSimpleClick.png");
    GUIContainer::createImage("imgDouble","btnDoubleClick.png");
    
    GUIContainer::getGUIImage("imgDouble")->setVisible(false);
    
    GUIContainer::getGUIImage("imgSimple")->setPosition(1070,260);
    GUIContainer::getGUIImage("imgDouble")->setPosition(1150,260);
    
    
    GUIContainer::createButton("btnExit", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "EXIT");
    GUIContainer::createButton("btnSave", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "SAVE");
    
    GUIContainer::getButton("btnExit")->setPosition(100,500);
    GUIContainer::getButton("btnSave")->setPosition(1070,500);
    
    
    GUIContainer::createImage("imgBlackBox","blackBox.png");
    GUIContainer::getGUIImage("imgBlackBox")->setPosition(683-400,384-200);

    GUIContainer::createTextField("save", "TextFieldBase.png", "NAME");
    GUIContainer::getTextField("save")->getText()->setText("");
    GUIContainer::getTextField("save")->setPosition(600, 300);
    GUIContainer::getTextField("save")->setTextPosition(18,18);
    GUIContainer::getTextField("save")->getText()->setCharHeight(0.025);
    GUIContainer::getTextField("save")->setOnlyUpper(true);
    GUIContainer::getTextField("save")->setMaxChar(20);
    GUIContainer::getTextField("save")->setSelected(true);

    GUIContainer::createText("txtSave","SAVE");
    GUIContainer::getText("txtSave")->setPosition(683, 250);
    GUIContainer::getText("txtSave")->setCharHeight(0.05);
    GUIContainer::getText("txtSave")->setAlignment(TextAreaOverlayElement::Center);

    GUIContainer::createButton("btnCreateSave", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "SAVE");
    GUIContainer::getButton("btnCreateSave")->setPosition(500,400);

    GUIContainer::createButton("btnCancel", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "CANCEL");
    GUIContainer::getButton("btnCancel")->setPosition(700,400);
    
    GUIContainer::getGUIImage("imgBlackBox")->setVisible(false);
    GUIContainer::getTextField("save")->setVisible(false);
    GUIContainer::getText("txtSave")->setVisible(false);
    GUIContainer::getButton("btnCreateSave")->setVisible(false);
    GUIContainer::getButton("btnCancel")->setVisible(false);
    
    
    
    GUIContainer::createSlider("sliRed","SliderSel.png", "SliderSel.png", "SliderSel.png", "SliderBar.png", "R:");
    GUIContainer::getSlider("sliRed")->setPosition(1070, 350);
    GUIContainer::getSlider("sliRed")->setTextPosition(-15,2);
    GUIContainer::getSlider("sliRed")->setValTextPosition(210,2);
    GUIContainer::getSlider("sliRed")->getText()->setColor(ColourValue::Red);
    GUIContainer::getSlider("sliRed")->setMinMax(0,255);
    GUIContainer::getSlider("sliRed")->setValRel(1);
    
    GUIContainer::createSlider("sliGreen","SliderSel.png", "SliderSel.png", "SliderSel.png", "SliderBar.png", "G:");
    GUIContainer::getSlider("sliGreen")->setPosition(1070, 370);
    GUIContainer::getSlider("sliGreen")->setTextPosition(-15,2);
    GUIContainer::getSlider("sliGreen")->setValTextPosition(210,2);
    GUIContainer::getSlider("sliGreen")->getText()->setColor(ColourValue::Green);
    GUIContainer::getSlider("sliGreen")->setMinMax(0,255);
    GUIContainer::getSlider("sliGreen")->setValRel(1);
    
    GUIContainer::createSlider("sliBlue","SliderSel.png", "SliderSel.png", "SliderSel.png", "SliderBar.png", "B:");
    GUIContainer::getSlider("sliBlue")->setPosition(1070, 390);
    GUIContainer::getSlider("sliBlue")->setTextPosition(-15,2);
    GUIContainer::getSlider("sliBlue")->setValTextPosition(210,2);
    GUIContainer::getSlider("sliBlue")->getText()->setColor(ColourValue::Blue);
    GUIContainer::getSlider("sliBlue")->setMinMax(0,255);
    GUIContainer::getSlider("sliBlue")->setValRel(1);
    
}

void CreateState::exit (){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void CreateState::pause(){
}

void CreateState::resume(){
  _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 0.0, 1.0));
}

bool CreateState::frameStarted(const Ogre::FrameEvent& evt){

    Console::setStatic("FPS", "FPS" ,1.0 / evt.timeSinceLastFrame);
    
    
    _r = GUIContainer::getSlider("sliRed")->getValRel();
    _g = GUIContainer::getSlider("sliGreen")->getValRel();
    _b = GUIContainer::getSlider("sliBlue")->getValRel();
    
    if(static_cast<Base*>(_gom->getObject(0))->getCubes()[0]->getColor() != Vector3(_r,_g,_b)){
        static_cast<Base*>(_gom->getObject(0))->getCubes()[0]->setColor(_r,_g,_b,1);
    }
    
    return true;
}

bool CreateState::frameEnded(const Ogre::FrameEvent& evt){
    
    
    GUIContainer::update();
    
    if (_exitGame)
        return false;
  
    return true;
}

void CreateState::keyPressed(const OIS::KeyEvent &e){
    
    
    GUIContainer::keyPressed(e);
    if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        GUIContainer::getButton("btnExit")->enter(&CreateState::menu, *this);
    }
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    
    if (e.text == 186) {
        Console::setVisible(!Console::isVisible());
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
    }
}

void CreateState::keyReleased(const OIS::KeyEvent &e){
    
    /*if (e.key == OIS::KC_Y) {
        _exitGame = true;
    }*/
}

void CreateState::mouseMoved(const OIS::MouseEvent &e){
    
    GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
    
    Console::setStatic("mouseX","X",e.state.X.abs);
    Console::setStatic("mouseY","Y",e.state.Y.abs);
    if(!_save){
        if(e.state.X.abs > 430 && e.state.X.abs < 964 && e.state.Y.abs > 75 && e.state.Y.abs < 585){
            if(_double){
                if(e.state.X.abs < 940){
                    static_cast<Base*>(_gom->getObject(0))->setCubePosition(0,(e.state.X.abs-430)*0.043,(e.state.Y.abs-75)*0.043);
                }
            }else{
                static_cast<Base*>(_gom->getObject(0))->setCubePosition(0,(e.state.X.abs-430)*0.043,(e.state.Y.abs-75)*0.043);
            }

        }
    }
    //static_cast<Base*>(_gom->getObject(0))->getCubes()[0]->setPosition((int)((e.state.X.abs-430)*0.04),(int)(-(e.state.Y.abs-75)*0.04));
}

void CreateState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
    GUIContainer::click(id);
    
    if(id == OIS::MB_Left){
        if(GUIContainer::getButton("btnSimple")->isClick()){
            GUIContainer::getGUIImage("imgSimple")->setVisible(true);
            GUIContainer::getGUIImage("imgDouble")->setVisible(false);
            _double = false;
        }
        if(GUIContainer::getButton("btnDouble")->isClick()){
            GUIContainer::getGUIImage("imgSimple")->setVisible(false);
            GUIContainer::getGUIImage("imgDouble")->setVisible(true);
            _double = true;
        }
    }
    
    if(!_save){
        static_cast<Base*>(_gom->getObject(0))->getCubes()[0]->setDouble(_double);

        if(id == OIS::MB_Left && e.state.X.abs > 430 && e.state.X.abs < 964 && e.state.Y.abs > 75 && e.state.Y.abs < 585){
            if(_double){
                if(e.state.X.abs < 940){
                    static_cast<Base*>(_gom->getObject(0))->createCube((e.state.X.abs-430)*0.043,(e.state.Y.abs-75)*0.043, _double, _r,_g,_b);
                }
            }else{
                static_cast<Base*>(_gom->getObject(0))->createCube((e.state.X.abs-430)*0.043,(e.state.Y.abs-75)*0.043, _double, _r,_g,_b);
            }
        }

        if(id == OIS::MB_Right){
            static_cast<Base*>(_gom->getObject(0))->destroyCube((e.state.X.abs-430)*0.043,(e.state.Y.abs-75)*0.043);
        }
    }
}

void CreateState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
    
    if(id == OIS::MB_Left){
        GUIContainer::getButton("btnExit")->click(&CreateState::menu, *this);
        GUIContainer::getButton("btnSave")->click(&CreateState::save, *this);
        GUIContainer::getButton("btnCreateSave")->click(&CreateState::overSave, *this);
        GUIContainer::getButton("btnCancel")->click(&CreateState::cancel, *this);
    }
    GUIContainer::clickReleased(id);
}

CreateState* CreateState::getSingletonPtr (){
    return msSingleton;
}

CreateState& CreateState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}


void CreateState::menu() {
    _simpleEffect->play();
    GUIContainer::setAllvisible(false);
    _gom->clear();
    
    pushState(MenuState::getSingletonPtr());
    
}

void CreateState::save() {

    _save = true;
    
    
    GUIContainer::getGUIImage("imgBlackBox")->setVisible(true);
    GUIContainer::getTextField("save")->setVisible(true);
    GUIContainer::getText("txtSave")->setVisible(true);
    GUIContainer::getButton("btnCreateSave")->setVisible(true);
    GUIContainer::getButton("btnCancel")->setVisible(true);
}

void CreateState::overSave() {
    
    static_cast<Base*>(_gom->getObjectByName("create"))->destroyCube(0);
    
    char c[20];
    for(int i=0; i<20; i++){
        c[i] = GUIContainer::getTextField("save")->getText()->getText()[i];
    }
    
    Data* d = new Data();
    d->addStage(c, static_cast<Base*>(_gom->getObjectByName("create"))->getCubesData());
    
    menu();
    //std::vector<std::string> ss = d->getNames();ppp
    //std::vector<Data::Point> dada = d->getStage("Prueba");
}

void CreateState::cancel() {
    GUIContainer::getGUIImage("imgBlackBox")->setVisible(false);
    GUIContainer::getTextField("save")->setVisible(false);
    GUIContainer::getText("txtSave")->setVisible(false);
    GUIContainer::getButton("btnCreateSave")->setVisible(false);
    GUIContainer::getButton("btnCancel")->setVisible(false);
}
