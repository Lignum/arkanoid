#include "Arkanoid/GameObjectManager.h"

GameObjectManager::GameObjectManager(SceneManager* sce) {
    _sceneMgr = sce;
}

void GameObjectManager::createObject(const String& name, const String& mesh, Real x, Real y, Real z) {

    _gameObjects.push_back(new GameObject(_sceneMgr, name, mesh, x, y, z));
   
}

void GameObjectManager::addObject(GameObject* object) {
    _gameObjects.push_back(object);
}


GameObject* GameObjectManager::getObjectByName(const String& name) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        if(_gameObjects[i]->getName() == name){
            return _gameObjects[i];
        }
    }
    
    return NULL;
}

GameObject* GameObjectManager::getObject(const int num) {
    return _gameObjects[num];
}

std::vector<GameObject*> GameObjectManager::getObjects() {
    return _gameObjects;
}

void GameObjectManager::clear() {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->destroy();
        delete _gameObjects[i];
    }
    _gameObjects.clear();
}

void GameObjectManager::destroyObject(int i) {
    _gameObjects[i]->destroy();
    _gameObjects.erase(_gameObjects.begin()+i);
}

void GameObjectManager::destroyObject(int start, int end) {
    for(int i=start; i<end; i++){        
        _gameObjects[i]->destroy();
    }
    _gameObjects.erase(_gameObjects.begin()+start, _gameObjects.begin()+end);
}

void GameObjectManager::setVisible(bool visible) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->setVisible(visible);
    }
}


void GameObjectManager::update(Real delta) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->update(delta);
    }
}

void GameObjectManager::keyPressed(const OIS::KeyEvent& e) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->keyPressed(e);
    }
}

void GameObjectManager::keyReleased(const OIS::KeyEvent& e) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->keyReleased(e);
    }
}

SceneManager* GameObjectManager::getSceneManager() {
    return _sceneMgr;
}

