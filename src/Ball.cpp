#include "Arkanoid/Ball.h"
#include "GUI/Console.h"

Ball::Ball(SceneManager* sce, Real x, Real y) : GameObject(sce, "Bola.mesh", x, y, 0.1) {
    init();
}

Ball::Ball(GameObjectManager* gom, Real x, Real y) : GameObject(gom, "Bola.mesh", x, y, 0.1) {
    init();
}

Ball::~Ball() {
    
}


void Ball::init() {
    _speed = 0;
    _dir = Vector2(0,1);
    _dir.normalise();
    _lastCol = -1;
    
    _sticky = -1000;
    
    getBoundingBox().setMaximum(0.5,0.5,0.5);
    
}


void Ball::update(Real delta) {
    
    if(_sticky == -1000){
    
        translate(_dir*_speed*delta);
    }
    
    if(_speed != 0){
        if(_speed < 21){
            _speed += 0.002;
        }else{
            _speed = 21;
        }
    }
    
    Console::setStatic("speed","Speed",_speed);
}

void Ball::start() {
    _speed = 10;
}

void Ball::start(Real speed, Vector2 dir) {
    _speed = speed;
    _dir = dir;
    
    _dir.normalise();
}

void Ball::setPosition(Real x, Real y) {    
    GameObject::setPosition(x, y, 0);
}

void Ball::translate(Real x, Real y) {
    GameObject::translate(x, y, 0);
}

void Ball::translate(Vector2 vec) {
    Vector3 v(vec.x, vec.y, 0);
    GameObject::translate(v);
}

int Ball::collide(GameObject* object, bool repit) {
  
    
    int res = -1;
    
    
    if(sphereCollide(object)){

        
        
        AxisAlignedBox ax = getBoundingBox().intersection(object->getBoundingBox());
        //std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaa: " << _lastCol << std::endl;
        
        if(!ax.isNull()){
            
            if(ax.getSize().y < ax.getSize().x){

                //UP
                if(getBoundingBox().getCenter().y < ax.getCenter().y && _dir.y>0){
                    res = 0;
                }

                //DOWN
                if(getBoundingBox().getCenter().y > ax.getCenter().y && _dir.y<0){
                    res = 1;
                }
            }else{

                //RIGHT
                if(getBoundingBox().getCenter().x < ax.getCenter().x && _dir.x>0){
                    res = 2;
                }

                //LEFT
                if(getBoundingBox().getCenter().x > ax.getCenter().x && _dir.x<0){
                    res = 3;
                }
            }
            
            _lastCol = res;
        }

    }
    
    return res;
}

Real Ball::getSpeed() {
    return _speed;
}

Real Ball::getSticky() {
    return _sticky;
}

void Ball::setSticky(Real sticky) {
    _sticky = sticky;
}
/*
void Ball::setSticky() {
    _sticky = true;
}

bool Ball::isSticky() {
    return _sticky;
}
*/
void Ball::setDirection(Vector2 vec) {
    _dir = vec;
}

Vector2 Ball::getDirection() {
    return _dir;
}

void Ball::addDirection(Vector2 vec) {
    _dir = _dir + vec;
    
    //_dir = _dir.normalisedCopy();
    if(_dir.y<0.1){
        _dir.y = 0.1;
    }
    _dir.normalise();
    
}

void Ball::bounce(Vector2 vec) {
    _dir = _dir.reflect(vec);
    
    //std::cout << _dir << std::endl;
}




