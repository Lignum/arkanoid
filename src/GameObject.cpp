#include <string>

#include "Arkanoid/GameObject.h"
#include "Arkanoid/GameObjectManager.h"

GameObject::GameObject(SceneManager* sce, const String& mesh) {
    init(sce, mesh, 0, 0, 0);
}

GameObject::GameObject(SceneManager* sce, const String& mesh, Real x, Real y, Real z) {
    init(sce, mesh, x, y, z);
}

GameObject::GameObject(GameObjectManager* gom, const String& mesh){
    init(gom->getSceneManager(), mesh, 0, 0, 0);
    gom->addObject(this);
}

GameObject::GameObject(GameObjectManager* gom, const String& mesh, Real x, Real y, Real z){
    init(gom->getSceneManager(), mesh, x, y, z);
    gom->addObject(this);
}

GameObject::GameObject(SceneManager* sce, const String& name, const String& mesh){
    init(sce, name, mesh, 0, 0, 0);
}

GameObject::GameObject(SceneManager* sce, const String& name, const String& mesh, Real x, Real y, Real z){
    init(sce, name, mesh, x, y, z);
}

GameObject::GameObject(GameObjectManager* gom, const String& name, const String& mesh){
    init(gom->getSceneManager(), name, mesh, 0, 0, 0);
    gom->addObject(this);
}

GameObject::GameObject(GameObjectManager* gom, const String& name, const String& mesh, Real x, Real y, Real z){
    init(gom->getSceneManager(), name, mesh, x, y, z);
    gom->addObject(this);
}

GameObject::~GameObject() {
}


void GameObject::init(SceneManager* sce, const String& mesh, Real x, Real y, Real z) {
    _sceneMgr = sce;    
    _node = sce->createSceneNode();
    Ogre::Entity* ent = sce->createEntity(mesh);
    ent->setCastShadows(true);
    _node->attachObject(ent);
    _node->setPosition(x,y,z);
    sce->getRootSceneNode()->addChild(_node);
    
    _color = Vector3(1,1,1);
    
    _destroyed = false;
}

void GameObject::init(SceneManager* sce, const String& name, const String& mesh, Real x, Real y, Real z){
    _sceneMgr = sce;    
    _node = sce->createSceneNode(name);
    Ogre::Entity* ent = sce->createEntity(mesh);
    ent->setCastShadows(true);
    _node->attachObject(ent);
    _node->setPosition(x,y,z);
    sce->getRootSceneNode()->addChild(_node);
    
    _color = Vector3(1,1,1);
    
    _destroyed = false;
    
}

void GameObject::update(Real delta) {
}

void GameObject::keyPressed(const OIS::KeyEvent& e) {

}

void GameObject::keyReleased(const OIS::KeyEvent& e) {

}

void GameObject::setPosition(Real x, Real y, Real z){
    _node->setPosition(x,y,z);
}

void GameObject::setScale(Real x, Real y, Real z) {
    _node->setScale(x,y,z);
}

void GameObject::translate(Real x, Real y, Real z){
    _node->translate(x,y,z);
}

void GameObject::translate(Vector3& vec) {
    _node->translate(vec);
}

Real GameObject::distance(GameObject* g) {
    return _node->getPosition().distance(g->_node->getPosition());
}

void GameObject::destroy() {
    
    _node->detachAllObjects();
    for(long unsigned int i = 0; i<_childs.size(); i++){
        _childs[i]->destroy();
    }
    
    _destroyed = true;
    //delete _node;
    //_node = NULL;
}

bool GameObject::isDestroyed() {
    return _destroyed;
}



void GameObject::setColor(Real r, Real g, Real b, Real a) {
    
    _color = Vector3(r,g,b);
    
    MaterialPtr mat = MaterialManager::getSingleton().create(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterialName()
            ,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);    
    
    mat.getPointer()->getTechnique(0)->getPass(0)->createTextureUnitState(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterial().getPointer()->
        getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureName());
    
    mat.getPointer()->setAmbient(r,g,b);
    mat.getPointer()->setDiffuse(r,g,b,a);
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->getSubEntity(0)->setMaterial(mat);
}

void GameObject::setTexture(String texture) {
    
    MaterialPtr mat = MaterialManager::getSingleton().create(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterialName()
            ,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);    
    
    mat.getPointer()->getTechnique(0)->getPass(0)->createTextureUnitState(texture);
    
    
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->getSubEntity(0)->setMaterial(mat);
    
}

void GameObject::setVisible(bool visible) {
    _node->setVisible(visible, true);
}


void GameObject::addChild(GameObject* go){    
    
    _sceneMgr->getRootSceneNode()->removeChild(go->getNode());
    _node->addChild(go->getNode());
    
    _childs.push_back(go);
}

GameObject* GameObject::getChild(const String& name) {
    for(long unsigned int i = 0; i<_childs.size(); i++){
        if(_childs[i]->getName() == name){
            return _childs[i];
        }
    }
    
    return NULL;
}

GameObject* GameObject::getChild(int num){
    return _childs[num];
}

std::vector<GameObject*> GameObject::getChilds() {
    return _childs;
}


bool GameObject::collide(GameObject* object) {
    if(getBoundingBox().intersects(object->getBoundingBox())){
        return true;
    }
    return false;
}

bool GameObject::sphereCollide(GameObject* object) {
    if(getBoundingSfere().intersects(object->getBoundingBox())){
        
        return true;
    }
    return false;
}


AxisAlignedBox GameObject::collision(GameObject* object) {
    return getBoundingBox().intersection(object->getBoundingBox());
}


const Vector3& GameObject::getPosition(){
    return _node->getPosition();
}

const Vector3& GameObject::getScale() {
    return _node->getScale();
}

const Vector3& GameObject::getColor() {
    return _color;
}



const String& GameObject::getName() {
    return _node->getName();
}

void GameObject::setCastShadows(bool shadow) {
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->setCastShadows(shadow);
}


Vector3 GameObject::getSize() {
    return getBoundingBox().getSize();
}


AxisAlignedBox GameObject::getBoundingBox() {
    return static_cast<Entity*>(_node->getAttachedObject(0))->getWorldBoundingBox();
}

Sphere GameObject::getBoundingSfere() {
    return static_cast<Entity*>(_node->getAttachedObject(0))->getWorldBoundingSphere();
}



SceneNode* GameObject::getNode(){
    return _node;
}

SceneManager* GameObject::getSceneManager() {
    return _sceneMgr;
}

