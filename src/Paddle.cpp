#include "Arkanoid/Paddle.h"
#include "GUI/Console.h"

Paddle::Paddle(SceneManager* sce) : GameObject(sce, "Paddle.mesh", 9,-20,0){
    _speed = 9;
    _dir=0;
    
    _sticky = false;
    
    _doubleTime = 0;
    _stickyTime = 0;
}

void Paddle::update(Real delta) {
    if(_dir <0 && (getPosition().x - getSize().x/2) < 0){
        _dir = 0;
    }
    if(_dir >0 && (getPosition().x + getSize().x/2) > 17){
        _dir = 0;
    }
    translate(_dir*_speed*delta,0,0);
    
    if(_doubleTime>0){
        _doubleTime -= delta;
        if(_doubleTime<=0){
            setScale(1,1,1);
        }
    }
    
    if(_stickyTime>0){
        _stickyTime -= delta;
        if(_stickyTime<=0){
            _sticky = false;
            setTexture("Paddle.jpg");
        }
    }
}

void Paddle::keyPressed(const OIS::KeyEvent& e) {
    
    if(e.key == OIS::KC_LEFT){
        _dir = -1;
    }
    if(e.key == OIS::KC_RIGHT){
        _dir = 1;
    }
}

void Paddle::keyReleased(const OIS::KeyEvent& e) {
    if(e.key == OIS::KC_LEFT && _dir == -1){
        _dir = 0;
    }
    if(e.key == OIS::KC_RIGHT && _dir == 1){
        _dir = 0;
    }
}

void Paddle::powerUp(int type) {
    switch(type){
        case PowerUp::T_LONG:
            _doubleTime = 30;
            if(getScale().x == 1){
                setScale(1.5,1,1);
            }
            break;
        case PowerUp::T_STICKY:
            _sticky = true;
            _stickyTime = 30;
            setTexture("PaddleSticky.jpg");
            break;
    }
}

bool Paddle::isSticky() {
    return _sticky;
}

