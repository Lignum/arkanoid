#include "MenuState.h"
#include "GUI/Console.h"

#include "Arkanoid/GameObject.h"
#include "Arkanoid/Base.h"
#include "Arkanoid/Cube.h"
#include "Arkanoid/Data.h"
#include "IntroState.h"
#include "PauseState.h"
#include "PlayState.h"
#include "CreateState.h"



template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void
MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    //_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _exitGame = false;
    
    createGui();
    
    _pTrackManager = TrackManager::getSingletonPtr();
    _pSoundFXManager = SoundFXManager::getSingletonPtr();
            
    //_mainTrack = _pTrackManager->load("137290__fran-ky__126-numerology-metal-3-c.wav");
    
    //_mainTrack->play();
    _simpleEffect = _pSoundFXManager->load("MetalPing.aiff");
    
    
}

void MenuState::createGui() {

    
    GUIContainer::showCursor(true);
    
    
    GUIContainer::createButton("btnPlay", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "PLAY");
    GUIContainer::createButton("btnCreate", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "CREATE");
    GUIContainer::createButton("btnScore", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "SCORE");
    GUIContainer::createButton("btnCredits", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "CREDITS");
    GUIContainer::createButton("btnExit", "btnRedBase.png", "btnRedFocus.png", "btnRedClick.png", "EXIT");
    
    GUIContainer::getButton("btnPlay")->setPosition(100, 100);
    GUIContainer::getButton("btnCreate")->setPosition(100, 200);
    GUIContainer::getButton("btnScore")->setPosition(100, 300);
    GUIContainer::getButton("btnCredits")->setPosition(100, 400);
    GUIContainer::getButton("btnExit")->setPosition(100, 500);

    GUIContainer::setDefaultTextCharHeight(0.03);
    GUIContainer::setDefaultTextFont("Blue");
}

void MenuState::exit (){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void MenuState::pause(){
}

void MenuState::resume(){
  _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 0.0, 1.0));
}

bool MenuState::frameStarted(const Ogre::FrameEvent& evt){

    Console::setStatic("FPS", "FPS" ,(int)(1.0 / evt.timeSinceLastFrame));
    
    return true;
}

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
    
    
    GUIContainer::update();
    
    
    if (_exitGame)
        return false;
  
    return true;
}

void MenuState::keyPressed(const OIS::KeyEvent &e){
    
    
    GUIContainer::keyPressed(e);
    if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        GUIContainer::getButton("btnPlay")->enter(&MenuState::play, *this);
        GUIContainer::getButton("btnCreate")->enter(&MenuState::create, *this);
        GUIContainer::getButton("btnScore")->enter(&MenuState::showScore, *this);
        GUIContainer::getButton("btnCredits")->enter(&MenuState::showCredits, *this);        
        GUIContainer::getButton("btnExit")->enter(&MenuState::closeGame, *this);
    }
    
     /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    
    if (e.text == 186) {
        Console::setVisible(!Console::isVisible());
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
    }
}

void MenuState::keyReleased(const OIS::KeyEvent &e){
    
    if (e.key == OIS::KC_Y) {
        _exitGame = true;
    }
}

void MenuState::mouseMoved(const OIS::MouseEvent &e){
    
    GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
}

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
    GUIContainer::click(id);
}

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    if(id == OIS::MB_Left){
        GUIContainer::getButton("btnPlay")->click(&MenuState::play, *this);
        GUIContainer::getButton("btnCreate")->click(&MenuState::create, *this);
        GUIContainer::getButton("btnScore")->click(&MenuState::showScore, *this);
        GUIContainer::getButton("btnCredits")->click(&MenuState::showCredits, *this);   
        GUIContainer::getButton("btnExit")->click(&MenuState::closeGame, *this);
    }
}

MenuState* MenuState::getSingletonPtr (){
    return msSingleton;
}

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

void MenuState::play() {
    
    _simpleEffect->play();
    
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    
    GUIContainer::setAllvisible(false);
    
    changeState(PlayState::getSingletonPtr());
}

void MenuState::create() {
    
    _simpleEffect->play();
    
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    
    GUIContainer::setAllvisible(false);
    
    changeState(CreateState::getSingletonPtr());
}

void MenuState::closeGame() {
    _simpleEffect->play();
    _exitGame = true;
}

void MenuState::showScore() {
    
    _simpleEffect->play();
    
    GUIContainer::setAllvisible(false);
    createGui();
    
    GUIContainer::createText("txtScore", "SCORE");
    GUIContainer::getText("txtScore")->setPosition(650, 50);
    GUIContainer::getText("txtScore")->setCharHeight(0.05);
    GUIContainer::getText("txtScore")->setAlignment(TextAreaOverlayElement::Center);
    
    ReadWrite<Score> rw("score.sav");
    std::vector<Score> vec = rw.read();
    
    std::string name;
    std::string point;
    
    for(unsigned int i=0; i<vec.size(); i++){
        name = "score-" + static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();
       
        GUIContainer::createText(name, vec[i].name);
        GUIContainer::getText(name)->setText(vec[i].name);
        GUIContainer::getText(name)->setPosition(500, 100+(20*i));
        
        GUIContainer::createText(name+"line", "_______________________________________");
        GUIContainer::getText(name+"line")->setPosition(500, 102+(20*i));
        GUIContainer::getText(name+"line")->setColor(ColourValue(0.3,0.3,0.3));
        
        GUIContainer::createText(name+"num", static_cast<std::ostringstream*>( &(std::ostringstream() << i+1) )->str()+".");
        GUIContainer::getText(name+"num")->setPosition(490, 100+(20*i));
        GUIContainer::getText(name+"num")->setAlignment(TextAreaOverlayElement::Right);
        
        name += "-points";        
        point = static_cast<std::ostringstream*>( &(std::ostringstream() << vec[i].score) )->str();
        
        GUIContainer::createText(name, point);
        GUIContainer::getText(name)->setText(point);
        GUIContainer::getText(name)->setPosition(900, 100+(20*i));
        GUIContainer::getText(name)->setAlignment(TextAreaOverlayElement::Right);
    }
}

void MenuState::showCredits() {
    _simpleEffect->play();
    
    GUIContainer::setAllvisible(false);
    createGui();
    
    GUIContainer::createText("txtCredits", "CREDITS");
    GUIContainer::getText("txtCredits")->setPosition(650, 350);
    GUIContainer::getText("txtCredits")->setCharHeight(0.05);
    GUIContainer::getText("txtCredits")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txtCreate", "CREATED BY VICTOR ROLDAN ARMENGOL");
    GUIContainer::getText("txtCreate")->setPosition(650, 380);
    GUIContainer::getText("txtCreate")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txtEngine", "ENGINE: OGRE3D");
    GUIContainer::getText("txtEngine")->setPosition(650, 400);
    GUIContainer::getText("txtEngine")->setAlignment(TextAreaOverlayElement::Center);
    
    
    GUIContainer::createText("txt__", "_______________________________________");
    GUIContainer::getText("txt__")->setPosition(650, 420);
    GUIContainer::getText("txt__")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txtSound", "SOUNDS: www.freesound.org");
    GUIContainer::getText("txtSound")->setPosition(650, 440);
    GUIContainer::getText("txtSound")->setAlignment(TextAreaOverlayElement::Center);
    
    
    GUIContainer::createText("txttimgormly", "AUTHOR: timgormly");
    GUIContainer::getText("txttimgormly")->setPosition(650, 460);
    GUIContainer::getText("txttimgormly")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txtMetal", "Metal Ping");
    GUIContainer::getText("txtMetal")->setPosition(650, 480);
    GUIContainer::getText("txtMetal")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txttimgormly", "AUTHOR: timgormly");
    GUIContainer::getText("txttimgormly")->setPosition(650, 460);
    GUIContainer::getText("txttimgormly")->setAlignment(TextAreaOverlayElement::Center);
    
    GUIContainer::createText("txtMetal", "Metal Ping");
    GUIContainer::getText("txtMetal")->setPosition(650, 480);
    GUIContainer::getText("txtMetal")->setAlignment(TextAreaOverlayElement::Center);
    
    
}

